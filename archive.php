<?php
/*
Template Name: Archives
*/
?>
<?php get_header(); ?>

<div id="container">
	<div id="content" class="post-overview" role="main">
		<?php the_post(); ?>
		<?php the_content(); ?>
		
		<?php $posts = get_posts(); ?>
		<?php if (count($posts) > 0) { ?>
			<?php foreach (get_posts() as $post) { ?>
				<article class="row left pb-s">
					<div class="col-8">
						<?php echo the_post_thumbnail(); ?>
					</div>
					<div class="col-8 post-title">
						<h5><?php echo get_the_title(); ?></h5>
					</div>
					<div class="col-8 post-excerpt">
						<p><?php echo get_the_excerpt(); ?></p>
						<?php echo showReadMore(READ_MORE, get_permalink(), null, 'arrow', null); ?>
					</div>
				</article>
			<?php } ?>
		<?php } ?>
	</div><!-- #content -->
</div><!-- #container -->

<?php get_footer(); ?>
