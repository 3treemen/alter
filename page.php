<?php get_header(); ?>
    <main>
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <h1 class="assistive"><?php the_title(); ?></h1>
            <?php the_content('BTN_READMORE');?>
        <?php endwhile; ?>
    <?php endif; ?>
    </main>
<?php get_footer(); ?>
