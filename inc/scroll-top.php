<button class="btn-scroll-top">
    <span class="assistive"><?php echo BACK_TO_TOP;?></span>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="85.453" height="85.453" viewBox="0 0 85.453 85.453">
        <g transform="matrix(-1, 0.017, -0.017, -1, 85.453, 83.987)">
            <g class="cls-1">
                <circle cx="42" cy="42" r="41"/>
            </g>
            <g class="cls-2" transform="translate(0 -31.333)">
                <g transform="translate(-1390.217 -823.305)">
                    <path d="M4918.885,875.3v13.977" transform="translate(-3486.954 13)"/>
                    <path d="M4915.19,913.316l-4.918,4.548-4.918-4.548" transform="translate(-3478.418 -14.82)"/>
                </g>
            </g>
        </g>
    </svg>
</button>