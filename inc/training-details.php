<?php

function training_details($duration = '', $date = '', $location = '', $price = '') {
	if ($duration || $date || $location || $price) {
		$output = '<table class="training-details">';
			if ($duration) {
				$output .= '
					<tr>
					<th scope="row">Duur:</th>
					<td>' .$duration. '</td>
				</tr>';
			}
			if ($date) {
			$output .= '	
			<tr>
				<th scope="row">Start:</th>
				<td>' .$date. '</td>
			</tr>';
			}
			if ($location) {
			$output .= '	
			<tr>
				<th scope="row">Locatie:</th>
				<td>'.$location. '</td>
			</tr>';
			}
			if ($price) {
			$output .= '
			<tr>
				<th scope="row">Kosten:</th>
				<td>€' .$price. '-ecl. BTW</td>
			</tr>';
			}
		$output .= '	
		</table>';
	return $output;
	}
}
?>