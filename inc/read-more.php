
<?php
function showReadMore($txt = READ_MORE, $anchorLink = null, $buttonType = null, $class = null){
    $output = '';
    if ($anchorLink){
        $output .= '<a class="read-more-button '.$class.'" href="'.$anchorLink.'">';
        $output .= '<span class="read-more-button-text">'.$txt.' </span>';
        $output .= '</a>';
    } elseif($buttonType){
        $output .= '<button type="'.$buttonType.'" class="read-more-button '.$class.'">';
        $output .= '<span class="read-more-button-text">'.$txt.' </span>';
        $output .= $svg;
        $output .= '</button>';
    }
    
    return $output;
}

?>