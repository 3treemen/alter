<div class="modal-bg js-modal" id="signup-modal">
	<div class="modal-content">
		<h2 class="modal-title"><?php echo SIGN_UP_TRAINING;?></h2>
		<button class="modal-close-button">
			<span>+</span>
		</button>
		<div class="modal-body">
			<form action="" class="modal-form js-modal-form" novalidate>
				<div class="js-form-wrapper">
					<input type="hidden" name="type" value="sign-up">
					<input type="hidden" name="training" value="">
					<label class="assistive" for="signup-company-name">Bedrijfsnaam</label>
					<input type="text" name="company-name" id="signup-company-name" placeholder="Bedrijfsnaam">
					<div class="form-group-name">
						<label class="assistive" for="signup-first-name">Voornaam *</label>
						<input type="text" name="first-name" id="signup-first-name" placeholder="Voornaam *" required>
						<label class="assistive" for="signup-middle-name">Tussenvoegsel</label>
						<input type="text" name="middle-name" id="signup-middle-name" placeholder="Tussenvoegsel">
						<label class="assistive" for="signup-last-name">Achternaam *</label>
						<input type="text" name="last-name" id="signup-last-name" placeholder="Achternaam *" required>
					</div>
					<label class="assistive" for="signup-function">Functie</label>
					<input type="text" name="function" id="signup-function" placeholder="Functie">
					<label class="assistive" for="signup-email">E-mail *</label>
					<input type="email" name="email" id="signup-email" placeholder="E-mail *" required>
					<label class="assistive" for="signup-phone-number">Telefoonnummer</label>
					<input type="tel" name="phone-number" id="signup-phone-number" placeholder="Telefoonnummer">
					<div class="form-submit">
						<?php echo showReadMore("Aanmelden", null, "submit", "theme-5") ?>
					</div>
				</div>
				<div class="js-form-feedback form-feedback"></div>
			</form>
		</div>
	</div>
</div>