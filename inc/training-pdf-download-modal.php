<div class="modal-bg js-modal" id="download-modal">
	<div class="modal-content">
		<h2 class="modal-title"><?php echo DOWNLOAD_PDF_TRAINING;?></h2>
		<button class="modal-close-button">
			<span>+</span>
		</button>
		<div class="modal-body">
			<form action="" class="modal-form js-modal-form" novalidate>
				<div class="js-form-wrapper">
					<input type="hidden" name="type" value="download">
					<input type="hidden" name="training" value="">
					<div class="form-group-name">
						<label class="assistive" for="download-first-name">Voornaam *</label>
						<input type="text" name="first-name" id="download-first-name" placeholder="Voornaam *" required>
						<label class="assistive" for="download-middle-name">Tussenvoegsel</label>
						<input type="text" name="middle-name" id="download-middle-name" placeholder="Tussenvoegsel">
						<label class="assistive" for="download-last-name">Achternaam *</label>
						<input type="text" name="last-name" id="download-last-name" placeholder="Achternaam *" required>
					</div>
					<label class="assistive" for="download-email">E-mail *</label>
					<input type="email" name="email" id="download-email" placeholder="E-mail *" required>
					<div class="form-group-checkbox">
						<input type="checkbox" id="download-nl-subscr" name="newsletter">
						<label for="download-nl-subscr">Meld mij aan voor de nieuwsbrief</label>
					</div>
					<div class="form-submit">
						<?php echo showReadMore("Download bestand", null, "submit", "theme-5") ?>
					</div>
				</div>
				<div class="js-form-feedback form-feedback"></div>
			</form>
		</div>
	</div>
</div>