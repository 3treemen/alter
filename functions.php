<?php
$langFile = strtolower(get_template_directory().'/language/'.get_locale().'.php');
if(file_exists($langFile)){
    include $langFile;
}

/*
* Enable maintanace mode
*/
function wp_maintenance_mode(){
    if(!is_user_logged_in()){
        wp_die('<h1 style="color:red">Website under Maintenance</h1><br />We are performing scheduled maintenance. We will be back online shortly!');
    }
}
// add_action('get_header', 'wp_maintenance_mode');


/*
* Define navigation blocks
*/
if (function_exists('add_theme_support')) {
    add_theme_support('menus');
    add_theme_support('post-thumbnails');
    add_theme_support( 'disable-custom-colors' );
    add_theme_support( 'editor-color-palette' );
    add_theme_support( 'disable-custom-font-sizes' );
    add_theme_support( 'editor-font-sizes', array() );
}


function register_my_menus() {
	register_nav_menus(
		array(
            'main-menu' => __( 'Header' ),
            'login-menu' => __( 'Login' ),
			'footer-menu' => __( 'Footer' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/**
 * Register our sidebars and widgetized areas.
 */
function widgets_init() {    
    register_sidebar( array(
		'name'          => 'Footer text',
		'id'            => 'footer_txt',
		'before_widget' => '<div class="contact-block-information">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="information-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'widgets_init' );

/*
* Add custom image sizes
*/
add_image_size('full-width', 1920, 1000, false);
add_image_size('medium-width', 1050, 750, array('center', 'center'));
add_image_size('medium-width-square', 1050, 1050, array('center', 'center'));
add_image_size('widescreen-large', 1920, 1000, array('center', 'center'));
add_image_size('widescreen-medium', 770, 315, array('center', 'center'));
/*
* Remove the admin bar from the front-end
*/
function remove_admin_bar() {
	  show_admin_bar(false);
}
// add_action('after_setup_theme', 'remove_admin_bar');

/* 
* Remove unused admin menu items
*/

function remove_menu_items() {
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page('edit.php');
}

add_action( 'admin_menu', 'remove_menu_items' );


/*
* Load defailt scripts and set JS globals
*/
function enqueue_scripts() {
    wp_enqueue_script( 'main', get_template_directory_uri().'/assets/js/main.js','','',true );
    wp_localize_script( 'main', 'globals', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'theme_folder' => get_template_directory_uri()));
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

/*
* Remove default styles from Gutenberg blocks
*/
function remove_wp_block_library_css(){
	wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'remove_wp_block_library_css' );

/*
* Load custom editor CSS
*/
function load_custom_editor_style() {
    wp_register_style( 'custom_editor_css', get_template_directory_uri() . '/assets/css/editor.css');
    wp_enqueue_style( 'custom_editor_css' );
}
add_action( 'enqueue_block_editor_assets', 'load_custom_editor_style' );

/*
* Load custom admin CSS
*/
function load_custom_admin_style() {
    wp_register_style( 'custom_admin_css', get_template_directory_uri() . '/assets/css/admin.css');
    wp_enqueue_style( 'custom_admin_css' );
}
add_action( 'admin_init', 'load_custom_admin_style' );

add_filter('block_editor_settings', function ($editor_settings) {
    unset($editor_settings['styles'][0]);
    return $editor_settings;
});

/*
* Custom Gutenberg Block Registration Functions
*/
function load_custom_admin_assets() {

    wp_register_style('admin-editor-css',
        get_template_directory_uri() . '/assets/css/editor.css',
    );

    wp_enqueue_script('overwrite-core-blocks',
        get_template_directory_uri() . '/assets/js/gutenberg/overwrite-core-blocks.js',
        array('wp-hooks', 'wp-components', 'wp-compose', 'wp-block-editor', ),
    );

    wp_enqueue_script('header-block-extend',
        get_template_directory_uri() . '/assets/js/gutenberg/header-block-extend.js',
        array('wp-hooks', 'wp-components', 'wp-compose', 'wp-block-editor', ),
    );

    wp_enqueue_script('row-block',
        get_template_directory_uri() . '/assets/js/gutenberg/row-block.js',
        array('wp-hooks', 'wp-components', 'wp-compose', 'wp-block-editor', 'wp-dom-ready'),
    );

    register_block_type('cw/row-block', array(
        'editor_script' => 'row-block',
    ));

    wp_enqueue_script('column-block',
        get_template_directory_uri() . '/assets/js/gutenberg/column-block.js',
        array('wp-hooks', 'wp-components', 'wp-compose', 'wp-block-editor', ),
    );

    register_block_type('cw/column-block', array(
        'editor_script' => 'column-block'
    ));

    wp_register_script( 'button-block-js', 
        get_template_directory_uri() . '/assets/js/gutenberg/button-block.js', 
        array('wp-blocks', 'wp-hooks', 'wp-block-editor', 'wp-components', 'wp-element') 
    );

    register_block_type('cw/button-block', array(
        'editor_script' => 'button-block-js'
    ));

    wp_register_script( 'quote-block-js', 
        get_template_directory_uri() . '/assets/js/gutenberg/quote-block.js', 
        array('wp-blocks', 'wp-block-editor', 'wp-components', 'wp-element') 
    );

    register_block_type('cw/quote-block', array(
        'editor_script' => 'quote-block-js'
    )); 

    wp_register_script( 'divider-block-js', 
        get_template_directory_uri() . '/assets/js/gutenberg/divider-block.js', 
        array('wp-blocks', 'wp-block-editor', 'wp-components', 'wp-element') 
    );

    register_block_type('cw/divider-block', array(
        'editor_script' => 'divider-block-js'
    )); 
	
	wp_register_script('subtitle-block-js',
        get_template_directory_uri() . '/assets/js/gutenberg/subtitle-block.js',
        array('wp-blocks', 'wp-hooks', 'wp-block-editor', 'wp-components', 'wp-element')
    );

    register_block_type('cw/subtitle-block', array(
        'editor_script' => 'subtitle-block-js',
	));
	
	wp_register_script('icon-block-js',
        get_template_directory_uri() . '/assets/js/gutenberg/icon-block.js',
        array('wp-blocks', 'wp-hooks', 'wp-block-editor', 'wp-components', 'wp-element')
    );

    register_block_type('cw/icon-block', array(
        'editor_script' => 'icon-block-js',
	));
	
	wp_register_script('contact-block-js',
        get_template_directory_uri() . '/assets/js/gutenberg/contact-block.js',
        array('wp-blocks', 'wp-hooks', 'wp-block-editor', 'wp-components', 'wp-element')
    );

    register_block_type('cw/contact-block', array(
        'editor_script' => 'contact-block-js',
    ));
}

add_action( 'admin_init', 'load_custom_admin_assets'); 

/* 
* Custom post types functions
*/
include 'custom_post_types/blog.php';
include 'custom_post_types/references.php';
include 'custom_post_types/training.php';
include 'custom_post_types/testimonials.php';

/* 
* Read more button function
*/
include 'inc/read-more.php';
include 'inc/training-details.php';


?>
