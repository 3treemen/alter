<?php

// Our custom post type function
function create_blog_posttype() {
 
    register_post_type( 'blog',
    // CPT Options
        array(
            'labels' => array(
                'name' => "Blog",
                'singular_name' => "Blog item",
                "add_new_item" => "Nieuw item",
                "add_new" => "Item toevoegen",
                "edit_item" =>  "Bewerk item",
                "new_item"  => "Nieuw item",
                "view_item" => "Bekijk item"
            ),
            'public' => true,
            'menu_icon' => 'dashicons-welcome-write-blog',
            'has_archive' => false,
            'rewrite' => array('slug' => 'blog'),
            'show_in_rest' => true,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
				'excerpt',
				'custom-fields'
			),
			// 'template' => 
			// array(
			// 	array( "cw/row-block", 
            //         array("wrapperClass" => "wrapper ci-left ci-v3 ci-left-offset-50","rowClass" => "row pt-s pb-m","topPadding" => "pt-s","bottomPadding" => "pb-m","selectedVariation" => "two-columns-one-third-two-third"), 
            //         array(
            //             array( "cw/column-block", 
            //                 array("colClass" => "col-8","blockClass" => "col-8 v-align-top ")
            //             ),
            //             array( "cw/column-block", 
            //                 array("colClass" => "col-16","blockClass" => "col-16 v-align-top "), 
            //                 array( 
            //                     array("core/heading", array("level" => 2, "content" => "Blog Titel")),
			// 					array( "core/paragraph", array( "placeholder" => "Voer de tekst voor dit blogpost in")),
			// 					array( "core/paragraph", array( "placeholder" => "Paragraaf twee")),
			// 					array( "core/paragraph", array( "placeholder" => "Paragraaf drie")),
			// 					array( "core/paragraph", array( "placeholder" => "Paragraaf vier"))
            //                 ) 
            //             )
            //         ) 
			// 	)
			// )
        )
    );
}

add_action( 'init', 'create_blog_posttype' );

function display_dynamic_blog_block($block_attributes, $content){

	global $post;
	$current_post = $post->ID;

    $postArguments = array(
        'numberposts'      => $block_attributes['nrItems'],
        'category'         => 0,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => array(),
        'exclude'          => array($current_post),
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'blog',
        'suppress_filters' => true,
    );

    $posts = get_posts($postArguments);


    ob_start();
    include 'inc/render-blog-block.php';
    return ob_get_clean();
}

/*
* Custom Dynamic Gutenberg Blog Block
*/
function init_dynamic_blog_block() {

    $asset_file = include( get_template_directory() . '/assets/js/gutenberg/blog-block.asset.php');

    wp_register_script( 'blog-block-js', 
        get_template_directory_uri() . '/assets/js/gutenberg/blog-block.js', 
        array('wp-blocks', 'wp-block-editor', 'wp-components', 'wp-element', 'wp-data'),
        $asset_file['dependencies'],
        $asset_file['version']
    );

    register_block_type('cw/blog-block', array(
        'editor_script' => 'blog-block-js',
        'attributes'      => array(
            'blockTitle'    => array(
                'type'      => 'string',
                'default'   => '',
            ),
            'nrItems'    => array(
                'type'      => 'string',
                'default'   => '1',
			),
			'displayType' => array(
				'type' => 'string',
				'default' => 'horizontal'
			)
        ),
        'render_callback' => 'display_dynamic_blog_block',
    ));
}

add_action( 'init', 'init_dynamic_blog_block'); 

?>
