<?php
// CPT creation function
function create_testimonials_posttype() {
	// Registration function
	register_post_type("testimonial",
		// CPT options
		array(
			"labels" => array(
                "name" => "Testimonials",
                "singular_name" => "Testimonial",
                "add_new_item" => "Nieuw item",
                "add_new" => "Item toevoegen",
                "edit_item" =>  "Bewerk item",
                "new_item"  => "Nieuw item",
                "view_item" => "Bekijk item"
			),
			"public" => true,
            "menu_icon" => "dashicons-editor-quote",
            "publicly_queryable" => true,
            "has_archive" => false,
            "rewrite" => array("slug" => "testimonials"),
            "show_in_rest" => true,
            "hierarchical" => true,
            "supports" => array(
                "title",
                "editor",
                "thumbnail",
                'custom-fields'
			),
			"register_meta_box_cb" => "init_testimonials_admin_sidebar"
		)
	);
}

add_action("init", "create_testimonials_posttype");

/**
 * Enqueue and register script for the side bar in the post editor
 */
function init_testimonials_admin_sidebar() {
	wp_enqueue_script('testimonials-post-sidebar',
        get_template_directory_uri() . '/assets/js/gutenberg/testimonials-post-sidebar.js',
        array( 'wp-plugins', 'wp-edit-post', 'wp-element', 'wp-components', 'wp-data' )
    );
}

/**
 * Register meta fields - NOTE: Custom fields has to be added to supports in post registration 
 */
function register_testimonials_meta() {
	register_meta("post", "_show_testimonial_in_block", 
		array(
			"object_subtype" => "testimonial",
			"show_in_rest" => true,
			"type" => "boolean",
			"single" => true,
			"sanitize_callback" => "sanitize_text_field",
			"auth_callback" => function() { 
				return current_user_can("edit_posts");
			}
		)
	);

	register_meta("post", "_testimonial_content", 
		array(
			"object_subtype" => "testimonial",
			"show_in_rest" => true,
			"type" => "string",
			"single" => true,
			"sanitize_callback" => "sanitize_text_field",
			"auth_callback" => function() { 
				return current_user_can("edit_posts");
			}
		)
	);

	register_meta("post", "_testimonial_author", 
		array(
			"object_subtype" => "testimonial",
			"show_in_rest" => true,
			"type" => "string",
			"single" => true,
			"sanitize_callback" => "sanitize_text_field",
			"auth_callback" => function() { 
				return current_user_can("edit_posts");
			}
		)
	);

	register_meta("post", "_testimonial_author_company", 
		array(
			"object_subtype" => "testimonial",
			"show_in_rest" => true,
			"type" => "string",
			"single" => true,
			"sanitize_callback" => "sanitize_text_field",
			"auth_callback" => function() { 
				return current_user_can("edit_posts");
			}
		)
	);
}

add_Action("init", "register_testimonials_meta");

/**
 * Function to initialise testimonials gutenberg block and pass attributes for server side rendering
 */
function init_dynamic_testimonials_block() {
	// Get the PHP asset file for dependecies and versioning
	$asset_file = include( get_template_directory() . "/assets/js/gutenberg/testimonials-block.asset.php");

	// Register the JS file 
    wp_register_script( "testimonials-block-js", 
        get_template_directory_uri() . "/assets/js/gutenberg/testimonials-block.js", 
        array("wp-blocks", "wp-block-editor", "wp-components", "wp-element"),
        $asset_file["dependencies"],
        $asset_file["version"]
    );

	// Register the block like normal, and additionally pass attributes for server side rendering
    register_block_type("cw/testimonials-block", array(
        "editor_script" => "testimonials-block-js",
        "attributes"      => array(),
		// Passing a function which will generate block markup
        "render_callback" => "display_dynamic_testimonials_block",
    ));
}

add_Action("init", "init_dynamic_testimonials_block");

/**
 * Function to display testimonials dynamically in Gutenberg editor and Frontend
 */
function display_dynamic_testimonials_block($block_attributes, $content) {
	$postArguments = array(
        'numberposts'      => -1,
        'category'         => 0,
        'orderby'          => 'rand',
        'order'            => 'DESC',
        'include'          => array(),
        'exclude'          => array(),
        'meta_key'         => '_show_testimonial_in_block',
        'meta_value'       => true,
        'post_type'        => 'testimonial',
        'suppress_filters' => true,
    );

    $posts = get_posts($postArguments);

    ob_start();
    include 'inc/render-testimonials-block.php';
    return ob_get_clean();
}

include "inc/extend-testimonials-columns.php";
?>