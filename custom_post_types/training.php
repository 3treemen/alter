<?php
// Our custom post type function
function create_training_posttype() {

    register_post_type( 'training',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Trainingen' ),
                'singular_name' => __( 'Trainen' )
            ),
            'public' => true,
            'menu_icon' => 'dashicons-clipboard',
            'has_archive' => false,
            'rewrite' => array('slug' => 'training'),
            'show_in_rest' => true,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
				'excerpt',
				'custom-fields'
			),
			"register_meta_box_cb" => "init_training_sidebar"
        )
	);
}

add_action( 'init', 'create_training_posttype' );

/**
 * Custom Meta Boxes
 */

function init_training_sidebar() {
	wp_enqueue_script('training-post-sidebar',
        get_template_directory_uri() . '/assets/js/gutenberg/training-post-sidebar.js',
        array( 'wp-plugins', 'wp-edit-post', 'wp-element', 'wp-components', 'wp-data' )
    );
}

function register_training_meta() {
    register_meta('post', '_training_type', array(
        'object_subtype' => 'training',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
	));
    
    register_meta('post', '_training_show_in_block', array(
        'object_subtype' => 'training',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
	));
	
	register_meta('post', '_training_start_date', array(
        'object_subtype' => 'training',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
	));
	
	register_meta('post', '_training_duration', array(
        'object_subtype' => 'training',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
	));
	
	register_meta('post', '_training_location', array(
        'object_subtype' => 'training',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
	));
	
	register_meta('post', '_training_cost', array(
        'object_subtype' => 'training',
        'show_in_rest' => true,
        'type' => 'number',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
    ));

    register_meta('post', '_training_info_file', array(
        'object_subtype' => 'training',
        'show_in_rest' => true,
        'type' => 'number',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
    ));
}

add_action( 'init', 'register_training_meta' );

function register_training_dynamic_block() {
	$asset_file = include( get_template_directory() . '/assets/js/gutenberg/training-block.asset.php');

    wp_register_script( 'training-block-js', 
        get_template_directory_uri() . '/assets/js/gutenberg/training-block.js', 
        array('wp-blocks', 'wp-block-editor', 'wp-components', 'wp-element'),
        $asset_file['dependencies'],
        $asset_file['version']
    );

    register_block_type('cw/training-block', array(
		'editor_script' => 'training-block-js',
		'attributes' => array(
			'blockTitle' => array(
				'type' => 'string',
				'default' => ''
			),
			'nrItems' => array(
				'type' => 'string',
				'default' => '1'
			),
			'alignment' => array(
				'type' => 'string',
				'default' => 'left'
            ),
            'showSlider' => array(
				'type' => 'string',
				'default' => '1'
            ),
            'typeTraining' => array(
				'type' => 'string',
				'default' => 'all'
			)
		),
		'render_callback' => 'display_dynamic_training_block'
    ));
}

function display_dynamic_training_block($block_attributes, $content){

    $postArguments = array(
		'numberposts'     => $block_attributes['nrItems'],
        'category'         => 0,
        'orderby'          => 'rand',
        'order'            => 'DESC',
        'include'          => array(),
		'exclude'          => array(),
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'   => '_training_show_in_block',
                'value' => 'yes',
            )
        ),
        'post_type'        => 'training',
        'suppress_filters' => true,
    );

    if ($block_attributes['typeTraining'] !== 'all'){
        $typeFilter = array(
            'key'   => '_training_type',
            'value' => $block_attributes['typeTraining'],
        );
        array_push($postArguments['meta_query'],$typeFilter);
    }

	$posts = get_posts($postArguments);
	
	ob_start();
    include 'inc/render-training-block.php';
    return ob_get_clean();
    
}

add_action("init", "register_training_dynamic_block");

include "inc/extend-training-columns.php";



/**
 * Mailer function for training form
 */

add_action('wp_ajax_send_training_email', 'send_training_email');
add_action('wp_ajax_nopriv_send_training_email', 'send_training_email');

function send_training_email(){
    $mailSubject = '';
    $trainingTitle = '';
    $trainingDate = '';
    $succesNotice = '';

    if (empty($_POST["email"]) || (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL))) {
        $return['message'] = "Invalid email";
        wp_send_json_error( $return );
    }
    
    if (!empty($_POST["training"])){
        $training = get_post($_POST["training"]);
        $trainingTitle = $training->post_title;

        $rawdate = get_post_meta($training->ID, "_training_start_date", true);
		if ($rawdate){
			$trainingDate = strftime("%d %B %Y", strtotime($rawdate));
		}
        
        if ($_POST["type"] == 'sign-up'){
            $mailSubject = "Inschrijving ";
            $succesNotice = "<p>Uw inschrijving is verzonden.</p>";
        }
        
        if ($_POST["type"] == 'download'){
            $mailSubject = "Brochure download ";
        
            $brochureID = get_post_meta($training->ID, "_training_info_file", true);
            if ($brochureID) {
                $brochure = get_post($brochureID);
                $brochureURL = $brochure->guid;
            }

            $succesNotice = "<p>Bedankt voor uw interesse.</p><p><a target=\"_blank\" class=\"read-more-button theme-5\" href=".$brochureURL.">Download starten.</a></p>";
        }
    }

    $admin_email = get_option( 'admin_email' );
    $secondary_email = ('frank@cmtraining.nl');
    $to = $admin_email .','. $secondary_email; 
    $subject = $mailSubject." ". $trainingTitle ." ".$trainingDate;
    $headers = array('Content-Type: text/html; charset=UTF-8');
    
    $body = "";
    if (!empty($_POST["company-name"])){
        $body .= "Bedrijfsnaam: ".$_POST["company-name"]."<br/>"; 
    }

    if (!empty($_POST["first-name"]) || !empty($_POST["middle-name"]) || !empty($_POST["middle-name"])){
        $body .= "Naam:";
        if (!empty($_POST["first-name"])){
            $body .= " ".$_POST["first-name"];
        }
        if (!empty($_POST["middle-name"])){
            $body .= " ".$_POST["middle-name"];
        }
        if (!empty($_POST["last-name"])){
            $body .= " ".$_POST["last-name"];
        }
        $body .= "</br>";
    }
    if (!empty($_POST["email"])){
        $body .= "Email: ".$_POST["email"]."<br/>"; 
    }
    if (!empty($_POST["phone-number"])){
        $body .= "Telefoon: ".$_POST["phone-number"]."<br/>"; 
    }
    if (!empty($_POST["function"])){
        $body .= "Functie: ".$_POST["function"]."<br/>"; 
    }

    if (!empty($_POST["newsletter"])){
        $body .= "Aanmelden nieuwsbrief: Ja <br/>"; 
    }

    if (wp_mail( $to, $subject, $body, $headers )){
        $return['message'] = $succesNotice;
        wp_send_json_success( $return );
    } else {
        $return['message'] = "Something went wrong when sending the email";
        wp_send_json_error( $return );
    }
}


?>
