<?php
if (!empty($posts)) {
?>
	<section class="testimonials-block">
<?php
		foreach($posts as $post){
			$testimonial = get_post_meta($post->ID, "_testimonial_content", true);
			$testimonial_author = get_post_meta($post->ID, "_testimonial_author", true);
			$testimonial_author_company = get_post_meta($post->ID, "_testimonial_author_company", true);
		?>
			<div class="testimonial">
				<blockquote class="testimonial-content">
					<span><?php echo $testimonial ?></span>
				</blockquote>
				<p class="testimonial-author"><?php echo $testimonial_author ?>, <?php echo $testimonial_author_company ?></p>
			</div>
		<?php
		}
		?>
	</section>
<?php	
} 
?>