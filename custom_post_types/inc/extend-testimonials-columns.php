<?php
/**
 * This is a partial for extending the columns for the testimonial post type overview in the admin area of WP
 */

/**
 * Add new columns to the testimonials post overview in the admin
 */
function add_columns_to_testimonials_overview($columns) {
	$columns["shown_in_block"] = "Shown in Block";

	return $columns;
}

add_filter("manage_testimonial_posts_columns", "add_columns_to_testimonials_overview" );

/**
 * Add content to the new columns on the testimonials post overview in the admin
 */
function add_content_to_testimonials_columns($column, $post_id) {
	// Populate shown in block column
	if($column === "shown_in_block") {
		$showing = get_post_meta($post_id, "_show_testimonial_in_block", true);
		$value = '';
		if ($showing) {
			$value = "Yes";
		} else {
			$value = "No";
		}
		echo $value;
	}
}

add_action("manage_testimonial_posts_custom_column", "add_content_to_testimonials_columns", 10, 2);

/**
 * Add columns to list of sortable columns
 */
function make_testimonial_columns_sortable($columns) {
	$columns["shown_in_block"] = "_show_testimonial_in_block";

	return $columns;
}

add_filter("manage_edit-testimonial_sortable_columns", "make_testimonial_columns_sortable");

/**
 * Create sorting logic for columns
 */

function testimonials_posts_overview_orderby( $query ) {
	if(!is_admin() || !$query->is_main_query()) {
		return;
	}

	if ("_show_testimonial_in_block" === $query->get("orderby")) {
		$query->set("orderby", "meta_value");
		$query->set("meta_key", "_show_testimonial_in_block");
	}
}

add_action("pre_get_posts", "testimonials_posts_overview_orderby");

?>