<?php
if (!empty($posts)) {
	$sliderClass = 'no-slider';
	if ($block_attributes['showSlider'] == '1'){
		$sliderClass = 'training-slider';
	}
?>
	<section class="training-block">
		<?php if ($block_attributes['blockTitle']) { ?>
			<h2 class="has-text-align-center"><?php echo $block_attributes['blockTitle'];?></h2>
		<?php } ?>
		<div class="training-container <?php echo $sliderClass;?>">
		<?php
		setlocale(LC_ALL, 'nl_NL');
		foreach($posts as $post){ 
			$ID = $post->ID;
			$rawdate = get_post_meta($ID, "_training_start_date", true);
			if ($rawdate) {
				$date = strftime("%d %B %Y", strtotime($rawdate));
			} else {
				$date = "In overleg";
			}
			$title = get_the_title($ID);
			$excerpt = get_the_excerpt($ID);
			$duration = get_post_meta($ID, "_training_duration", true);
			$location = ucfirst(get_post_meta($ID, "_training_location", true));
			$price = get_post_meta($ID, "_training_cost", true);
			$formatted_price = number_format($price, 0, ".", ",");
		?>
			<article class="training-item">
				<h3><?php echo $title ?></h3>
				<p><?php echo $excerpt ?></p>
				<div class="divider"></div>
				<?php echo training_details($duration, $date, $location, $formatted_price) ?>
				<div class="training-buttons">
					<?php echo showReadMore(READ_MORE_TRAINING, get_permalink($post), null,"white"); ?>
					<button class="read-more-button white-inverse js-open-modal" data-modal="#signup-modal" data-training="<?php echo $ID ?>">
						<span class="read-more-button-text"><?php echo SIGN_UP_TRAINING ?></span>
					</button>
				</div>
			</article>
		<?php	
		}
		?>
		</div>
	</section>
	<?php get_template_part("inc/training-signup-modal") ?>
<?php
}
?>