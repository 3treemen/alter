<?php
/**
 * This is a partial for extending the columns for the reference post type overview in the admin area of WP
 */

/**
 * Add new columns to the references post overview in the admin
 */
function add_columns_to_references_overview($columns) {
	$columns["shown_in_block"] = "Weergegeven in blok";
	$columns["referrer_name"] = "Naam";
	$columns["referrer_job_title"] = "Functie";
	$columns["referrer_quote"] = "Quote";

	return $columns;
}

add_filter("manage_references_posts_columns", "add_columns_to_references_overview");

/**
 * Add content to the new columns on the references post overview in the admin
 */
function add_content_to_references_columns($column, $post_id) {
	// Populate shown in block column
	if($column === "shown_in_block") {
		$showing = get_post_meta($post_id, "_referrer_show_in_block", true) === "true" ? "Ja" : "Nee";
		echo $showing;
	}

	if($column === "referrer_name") {
		$showing = get_post_meta($post_id, "_referrer_name", true);
		echo $showing;
	}

	if($column === "referrer_job_title") {
		$showing = get_post_meta($post_id, "_referrer_job_title", true);
		echo $showing;
	}

	if($column === "referrer_quote") {
		$showing = get_post_meta($post_id, "_referrer_quote", true);
		echo $showing;
	}
}

add_action("manage_references_posts_custom_column", "add_content_to_references_columns", 10, 2);

/**
 * Add columns to list of sortable columns
 */
function make_reference_columns_sortable($columns) {
	$columns["shown_in_block"] = "_referrer_show_in_block";

	return $columns;
}

add_filter("manage_edit-references_sortable_columns", "make_reference_columns_sortable");

/**
 * Create sorting logic for columns
 */

function references_posts_overview_orderby( $query ) {
	if(!is_admin() || !$query->is_main_query()) {
		return;
	}

	if ("_referrer_show_in_block" === $query->get("orderby")) {
		$query->set("orderby", "meta_value");
		$query->set("meta_key", "_referrer_show_in_block");
	}
}

add_action("pre_get_posts", "references_posts_overview_orderby");

?>