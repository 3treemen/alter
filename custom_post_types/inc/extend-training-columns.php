<?php
/**
 * This is a partial for extending the columns for the training post type overview in the admin area of WP
 */

/**
 * Add new columns to the training post overview in the admin
 */
function add_columns_to_training_overview($columns) {
	$columns["shown_in_block"] = "Weergegeven in blok";
	$columns["start_date"] = "Startdatum";
	$columns["duration"] = "Looptijd";
	$columns["location"] = "Locatie";
	$columns["price"] = "Prijs";

	return $columns;
}

add_filter("manage_training_posts_columns", "add_columns_to_training_overview" );

/**
 * Add content to the new columns on the training post overview in the admin
 */
function add_content_to_training_columns($column, $post_id) {
	// Populate shown in block column
	if($column === "shown_in_block") {
		$showing = get_post_meta($post_id, "_training_show_in_block", true);
		echo ucfirst($showing);
	}

	// Populate start data column
	if($column === "start_date") {
		$start = get_post_meta($post_id, "_training_start_date", true);
		echo preg_replace("/.{9}$/", "" , $start);
	}

	// Populate duration column
	if($column === "duration") {
		$duration = get_post_meta($post_id, "_training_duration", true);
		echo $duration;
	}

	// Populate location column
	if($column === "location") {
		$location = get_post_meta($post_id, "_training_location", true);
		echo ucfirst($location);
	}

	// Populate price column
	if($column === "price") {
		$price = get_post_meta($post_id, "_training_cost", true);

		if(!$price) {
			echo "N/A";
		} else {
			$formatted_price = number_format($price, 0, ".", ",");
			echo "€" . $formatted_price;
		}
	}
}

add_action("manage_training_posts_custom_column", "add_content_to_training_columns", 10, 2);

/**
 * Add columns to list of sortable columns
 */
function make_training_columns_sortable($columns) {
	$columns["shown_in_block"] = "_training_show_in_block";
	$columns["start_date"] = "_training_start_date";
	$columns["price"] = "_training_cost";

	return $columns;
}

add_filter("manage_edit-training_sortable_columns", "make_training_columns_sortable");

/**
 * Create sorting logic for columns
 */

function training_posts_overview_orderby( $query ) {
	if(!is_admin() || !$query->is_main_query()) {
		return;
	}

	if ("_training_show_in_block" === $query->get("orderby")) {
		$query->set("orderby", "meta_value");
		$query->set("meta_key", "_training_show_in_block");
	}

	if ("_training_start_date" === $query->get("orderby")) {
		$query->set("orderby", "meta_value");
		$query->set("meta_key", "_training_start_date");
	}

	if ("_training_cost" === $query->get("orderby")) {
		$query->set("orderby", "meta_value");
		$query->set("meta_key", "_training_cost");
		$query->set( 'meta_type', 'numeric' );
	}
}

add_action("pre_get_posts", "training_posts_overview_orderby");
?>