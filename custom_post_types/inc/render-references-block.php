<?php
if (!empty($posts)) {
	$randID = rand(10, 99);
?>
	<section class="references-block">
	<?php 
	if ($block_attributes['blockTitle'] != ''){ 
	?>
		<h2><?php echo $block_attributes['blockTitle'];?></h2>
	<?php 
	} 
	?>
	<div class="references-list">
		<?php
		foreach($posts as $post){
			$thumbnail_id = get_post_thumbnail_id( $post);
			$thumb_alt = get_post_meta($thumbnail_id, "_wp_attachment_image_alt", true); 
			$thumb_info = wp_get_attachment_image_src($thumbnail_id);
			$reference_company_name = get_the_title($post);
			$referrer_name = get_post_meta($post->ID, "_referrer_name", true);
			$referrer_job_title = get_post_meta($post->ID, "_referrer_job_title", true);
			$referrer_quote = get_post_meta($post->ID, "_referrer_quote", true);
			$block_background_color = get_post_meta($post->ID, "_referrer_block_background_color", true);
			$block_class = 'col-bg-blue-l';

			switch ($block_background_color) {
				case '#148fad':
					$block_class = 'col-bg-blue-l';
					break;
				case '#1073ae':
					$block_class = 'col-bg-blue-m';
					break;
				case '#2d4a71':
					$block_class = 'col-bg-blue-d';
					break;
				case '#1dbfb1':
					$block_class = 'col-bg-green-l';
					break;
				case '#21b5a2':
					$block_class = 'col-bg-green';
					break;
			}
		?>
			<article class="reference">
				<div class="reference-content  <?php echo $block_class;?>">
					<h3 class="reference-title"><?php echo $reference_company_name ?></h3>
					<figure>
						<img 
							src="<?php echo $thumb_info[0] ?>" 
							alt="<?php echo $thumb_alt ?>" 
							width="<?php echo $thumb_info[1] ?>" 
							height="<?php echo $thumb_info[2] ?>" 
							loading=lazy
							class="reference-image"
						>
					</figure>
					<p class="reference-item name"><?php echo $referrer_name ?></p>
					<p class="reference-item job-title"><?php echo $referrer_job_title ?></p>
					<p class="reference-item quote">"<?php echo $referrer_quote ?>"</p>
				</div>
			</article>
		<?php
		}
		?>
	</div>
	</section>
<?php 
} 
?>