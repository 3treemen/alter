<?php
if (!empty($posts)) {
?>
	<?php
	// Start horizontal layout
	if($block_attributes['displayType'] == 'horizontal') {
	?>
		<section class="blog-block-horizontal">
			<?php
			if ($block_attributes['blockTitle'] != '') {
			?>
			<div class="section-title">
				<h2><?php echo $block_attributes['blockTitle']; ?></h2>
			</div>
			<?php
			}
			?>
			<?php
			foreach($posts as $post){
				$post_data = get_the_date('d-m-Y', $post );
				$post_excerpt = get_the_excerpt($post);
				$post_permalink = get_the_permalink($post->ID);
				$thumbnail_id = get_post_thumbnail_id( $post);
				$thumb_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
				$thumb_info = wp_get_attachment_image_src($thumbnail_id, 'medium-width');
			?>
				<article class="blog-post">
					<a href="<?php echo $post_permalink;?>">		
						<div class="blog-post-text">
							<h3 class="blog-post-title"><?php echo $post->post_title; ?></h3>
							<span class="date"><?php echo $post_data; ?></span>
							<p><?php echo $post_excerpt ?></p>
						</div>
						<figure class="blog-post-image">
							<img src="<?php echo $thumb_info[0];?>" alt="<?php echo $thumb_alt;?>" width="<?php echo $thumb_info[1];?>" height="<?php echo $thumb_info[2];?>" loading="lazy"/>
						</figure>
					</a>
				</article>
			<?php
			}
			?>
		</section>
	<?php
	}
	?>

	<?php
	if($block_attributes['displayType'] == 'vertical') {
	?>
		<section class="blog-block-vertical ">
		<?php
			if ($block_attributes['blockTitle'] != '') {
			?>
			<div class="section-title">
				<h2><?php echo $block_attributes['blockTitle']; ?></h2>
			</div>
			<?php
			}
			?>
			<?php
			foreach($posts as $post){
				$post_data = get_the_date('d-m-Y', $post );
				$thumbnail_id = get_post_thumbnail_id( $post);
				$thumb_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
				$thumb_info = wp_get_attachment_image_src($thumbnail_id, 'widescreen-medium'); 
			?>
			<article class="blog-post">
				<div class="blog-post-inner">
				<?php
				if ($thumb_info) {
				?>
					<div class="blog-post-image-wrapper">
						<div class="blog-post-image" style="background-image: url(<?php echo $thumb_info[0] ?>);"></div>
					</div>
				<?php
				}
				?>
					<div class="blog-post-text">
						<div class="blog-post-title">
							<h3><?php echo $post->post_title; ?></h3>
						</div>
						<div class="blog-post-intro">
							<p><?php echo get_the_excerpt($post); ?></p>
							<?php 
								echo showReadMore(READ_MORE, get_permalink($post), null);
							?>
						</div>
					</div>
				</div>
			</article>
			<?php
			}
			?>
		</section>
	<?php
	}
	?>
<?php
}
?>