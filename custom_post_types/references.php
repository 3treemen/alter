<?php
// Our custom post type function
function create_references_posttype() {
    register_post_type( "references",
    // CPT Options
        array(
            "labels" => array(
                "name" => "Referenties",
                "singular_name" => "Referentie",
                "add_new_item" => "New item",
                "add_new" => "Add item",
                "edit_item" =>  "Edit item",
                "new_item"  => "New item",
                "view_item" => "View item"
            ),
            "public" => true,
            "menu_icon" => "dashicons-format-gallery",
            "publicly_queryable" => true,
            "has_archive" => false,
            "rewrite" => array("slug" => "references"),
            "show_in_rest" => true,
            "hierarchical" => true,
            "supports" => array(
                "title",
                "editor",
                "thumbnail",
                "excerpt",
                'custom-fields'
            ),
            "register_meta_box_cb" => "init_admin_references_sidebar"
        )
    );
}

add_action( "init", "create_references_posttype" );


/*
* Custom Meta boxes
*/
function init_admin_references_sidebar() {
    wp_enqueue_script('references-post-sidebar',
        get_template_directory_uri() . '/assets/js/gutenberg/references-post-sidebar.js',
        array( 'wp-plugins', 'wp-edit-post', 'wp-element', 'wp-components', 'wp-data' )
    );
}

function register_references_meta() {
    register_meta('post', '_referrer_show_in_block', array(
        'object_subtype' => 'references',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
    ));

    register_meta('post', '_referrer_block_background_color', array(
        'object_subtype' => 'references',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
    ));
    
    register_meta('post', '_referrer_name', array(
        'object_subtype' => 'references',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
    ));

    register_meta('post', '_referrer_job_title', array(
        'object_subtype' => 'references',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
	));
	
	register_meta('post', '_referrer_quote', array(
        'object_subtype' => 'references',
        'show_in_rest' => true,
        'type' => 'string',
        'single' => true,
        'sanitize_callback' => 'sanitize_text_field',
        'auth_callback' => function() { 
            return current_user_can('edit_posts');
        }
    ));
}
add_action( 'init', 'register_references_meta' );


function display_dynamic_references_block($block_attributes, $content){

    $postArguments = array(
        'numberposts'      => 8,
        'category'         => 0,
        'orderby'          => 'rand',
        'order'            => 'DESC',
        'include'          => array(),
        'exclude'          => array(),
        'meta_key'         => '_referrer_show_in_block',
        'meta_value'       => 'true',
        'post_type'        => 'references',
        'suppress_filters' => true,
    );

    $posts = get_posts($postArguments);

    ob_start();
    include 'inc/render-references-block.php';
    return ob_get_clean();
}

/*
* Custom Dynamic Gutenberg References Block
*/
function init_dynamic_references_block() {

    $asset_file = include( get_template_directory() . '/assets/js/gutenberg/references-block.asset.php');

    wp_register_script( 'references-block-js', 
        get_template_directory_uri() . '/assets/js/gutenberg/references-block.js', 
        array('wp-blocks', 'wp-block-editor', 'wp-components', 'wp-element'),
        $asset_file['dependencies'],
        $asset_file['version']
    );

    register_block_type('cw/references-block', array(
        'editor_script' => 'references-block-js',
        'attributes'      => array(
            'blockTitle'    => array(
                'type'      => 'string',
                'default'   => '',
            )
        ),
        'render_callback' => 'display_dynamic_references_block',
    ));
}

add_action( 'init', 'init_dynamic_references_block'); 

include "inc/extend-references-columns.php";
?>
