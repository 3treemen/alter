const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sourcemaps = require('gulp-sourcemaps');
const gulpStylelint = require('gulp-stylelint');
const mode = require('gulp-mode')({
    modes: ["production", "development"],
    default: "production",
    verbose: false
});


export async function lintSass() {
    return gulp.src(['_src/_scss/**/*.scss'])
    .pipe(
        gulpStylelint({
            reporters: [
                {formatter: 'string', console: true},
            ],
            failAfterError: false
        })
    );
}


export async function compileSass() {
    return gulp.src('_src/_scss/**/*.scss')
    .pipe(
        mode.development(
            sourcemaps.init()
        )
    )
    .pipe(
        sass({
            outputStyle: 'nested'
        }).on('error', sass.logError)
    )
    .pipe(
        postcss([autoprefixer()])
    )
    .pipe(
        mode.production(
            postcss([cssnano()])
        )
    )
    .pipe(
        mode.development(
            sourcemaps.write('.')
        )
    )
    .pipe(
        gulp.dest('assets/css')
    )
}

export async function watchSass() {
    return gulp.watch([
        '_src/_scss/**/*.scss'
    ],
    gulp.series('lintSass', 'compileSass'));
}