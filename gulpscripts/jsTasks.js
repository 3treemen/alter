/*global require*/
const gulp = require("gulp");
const concat = require("gulp-concat");
const eslint = require('gulp-eslint');
const uglify = require("gulp-uglify-es").default;

const mode = require("gulp-mode")({
    modes: ["production", "development"],
    default: "production",
    verbose: false
});

export async function concatJS() {
    // Main file
    gulp.src([
        "_src/_js/vendor/jquery-3.5.1.js",
        "_src/_js/components/navigation.js",
        "_src/_js/components/modal.js",
        "_src/_js/components/scroll-top-button.js",
        "_src/_js/vendor/slick.min.js",
		"_src/_js/components/carousel.js",
		"_src/_js/components/page-scroll.js",
		"_src/_js/components/form-submit.js"
    ])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(concat("main.js"))
    .pipe(
        mode.production(
            uglify()
        )
    )
    .pipe(
        gulp.dest("assets/js")
    );

    gulp.src([
        "_src/_js/vendor/slick.min.js",
        "_src/_js/components/carousel.js"
    ])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(concat("slick.js"))
    .pipe(
        mode.production(
            uglify()
        )
    )
    .pipe(
        gulp.dest("assets/js/editor")
    );
}

export async function watchJS() {
    return gulp.watch([
        "_src/_js/**/*.js",
        "!_src/_js/gutenberg/**/*.js"
    ],
    gulp.series("concatJS"));
}
