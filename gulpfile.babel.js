const gulp = require('gulp');
const mode = require('gulp-mode')({
    modes: ["production", "development"],
    default: "production",
    verbose: false
});

import {lintSass, compileSass, watchSass} from './gulpscripts/sassTasks';
import {concatJS, watchJS} from './gulpscripts/jsTasks';

exports.lintSass = lintSass;
exports.compileSass = compileSass;
exports.watchSass = watchSass;
exports.concatJS = concatJS;
exports.watchJS = watchJS;

exports.build = gulp.parallel(lintSass, compileSass, concatJS);
exports.develop = gulp.parallel(watchSass, watchJS);
