<?php

// Generic Labels
define ('READ_MORE', 'Lees meer');
define ('READ_MORE_TRAINING', 'Lees meer over deze training');
define ('SIGN_UP_TRAINING', 'Inschrijving aanvragen');
define ('DOWNLOAD_PDF_TRAINING', 'Download brochure');
define ('CLOSE', 'Sluiten');

// Navigation
define ('NAV_OPEN', 'Open menu');
define ('NAV_CLOSE', 'Sluit menu');
define ('BACK_TO_TOP', 'Terug naar boven');

// Newsletter registration
define ('NLSUBSCR_FORM_HEADER', 'Ontvang onze nieuwsbrief');
?>