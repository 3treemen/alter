<?php

// Generic Labels
define ('READ_MORE', 'Read more');
define ('READ_MORE_TRAINING', 'Read more about this training');
define ('SIGN_UP_TRAINING', 'Request registration');
define ('DOWNLOAD_PDF_TRAINING', 'Download brochure');
define ('CLOSE', 'Close');

// Navigation
define ('NAV_OPEN', 'Open navigation');
define ('NAV_CLOSE', 'Close navigation');
define ('BACK_TO_TOP', 'Back to the top of the page');

// Newsletter registration
define ('NLSUBSCR_FORM_HEADER', 'Subscribe Newsletter');
?>