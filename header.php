<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?> - <?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
        <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/source-sans-pro-v13-latin-regular.woff" as="font" crossorigin="anonymous" />
        <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/source-sans-pro-v13-latin-italic.woff" as="font" crossorigin="anonymous" />
        <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/source-sans-pro-v13-latin-600.woff" as="font" crossorigin="anonymous" />
        <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/source-sans-pro-v13-latin-600italic.woff" as="font" crossorigin="anonymous" />
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/icons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/icons/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/icons/site.webmanifest">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/icons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/icons/favicon.ico">
        <meta name="msapplication-TileColor" content="#2b5797">
        <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/assets/icons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class() ?>>
        <header class="wrapper js-header">
            
                <div class="row">
                    <div class="logo">
                        <a class="logo-container" href="<?php echo site_url(); ?>">
                            <span class="assistive"><?php bloginfo('name'); ?></span>
                        </a>
                    </div>
                    <nav class="header-nav">
                        <div class="navigation-backdrop js-navigation-backdrop"></div>
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location'    => 'main-menu',
                                    'container'         => 'ul',
                                    'menu_id'           => '',
                                    'menu_class'        => 'main-nav dropdown-navigation',
                                    'before'            => '',
                                    'after'             => ''
                                )
                            );
                        ?>
                    </nav>
                    <nav class="login-nav">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location'    => 'login-menu',
                                    'container'         => 'ul',
                                    'menu_id'           => '',
                                    'menu_class'        => '',
                                    'before'            => '',
                                    'after'             => ''
                                )
                            );
                        ?>
                    </nav>
                    <button class="navigation-toggler js-navigation-toggler">
                        <span class="js-nav-label assistive" data-open-label="<?php echo NAV_OPEN;?>" data-close-label="<?php echo NAV_CLOSE;?>" ><?php echo NAV_OPEN;?></span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 12 6">
                            <rect class="d" x="0" y="0" width="12" height="1"/>
                            <rect class="d" x="0" y="3" width="12" height="1"/>
                            <rect class="d" x="0" y="6" width="12" height="1"/>
                        </svg>
                    </button>
                </div>
            
        </div>
    </header>
