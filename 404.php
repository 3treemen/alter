<?php get_header(); ?>
	<section class="container main">
		<div class="row">
			<div class="col-md-12">
				<div class="block">
					<h1>Pagina niet gevonden</h1>
					<p>De pagina welke u probeert te bezoeken bestaat niet (meer).</p>
					<a class="go-back" href="<?php echo bloginfo('wpurl');?>">Naar de homepage</a>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>