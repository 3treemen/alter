<?php get_header(); ?>
<main>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <section class="content">
            <?php the_content(); ?>
        </section>
	<?php endwhile; ?>
<?php endif; ?>
</main>
<?php get_footer(); ?>