<?php get_header(); ?>
<main>
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
    <?php 
        $thumbnail_id = get_post_thumbnail_id( $post);
        $thumb_info = wp_get_attachment_image_src($thumbnail_id, 'medium-width'); 
    ?>
		<section class="wrapper custom-case-hero">
            <div class="container">
                <div class="row right">
                    <div class="col-6">
                        <h1 class="project-title">
                            <?php the_title() ?>
                        </h1>
                    </div>
                    <div class="col-6 no-gutter featured-image" style="background-image: url('<?php echo $thumb_info[0];?>')">

                    </div>
                </div>
            </div>
		</section>
		<div class="share-menu-row">
            <?php get_template_part("inc/share-menu"); ?>
        </div>
        <section class="content">
            <?php the_content(); ?>
        </section>
	<?php endwhile; ?>
<?php endif; ?>
</main>
<?php get_footer(); ?>