<?php get_header(); ?>
<main>
<?php if ( have_posts() ) : ?>
	<?php setlocale(LC_ALL, 'nl_NL'); ?>
    <?php while ( have_posts() ) : the_post(); ?>
	<?php 
		$ID = $post->ID;
		$title = get_the_title($ID);
        $thumbnail_id = get_post_thumbnail_id( $post);
		$thumb_info = wp_get_attachment_image_src($thumbnail_id, 'full-width');
		$rawdate = get_post_meta($ID, "_training_start_date", true);
		if ($rawdate){
			$date = strftime("%d %B %Y", strtotime($rawdate));
		} else {
			$date = '';
		}
		$excerpt = get_the_excerpt($ID);
		$duration = get_post_meta($ID, "_training_duration", true);
		$location = ucfirst(get_post_meta($ID, "_training_location", true));
		$price = get_post_meta($ID, "_training_cost", true);
		$formatted_price = number_format($price, 0, ".", ",");
		$brochureID = get_post_meta($ID, "_training_info_file", true);

		if ($brochureID) {
			get_template_part("inc/training-pdf-download-modal");
		}

    ?>
		<section class="wrapper">
			<?php get_template_part("inc/training-signup-modal") ?>
            <div class="training-hero"style="background-image: url('<?php echo $thumb_info[0];?>');" >
				<div class="overlay"></div>
				<div class="container">
					<div class="row">
						<div class="col-12 training-hero-header">
							<h1 class="title"><?php echo $title ?></h1>
							<span> <?php echo $date ?> </span>
						</div>
					</div>
				</div>
			</div>
			<div class="training-info-container">
				<div class="training-info">
					<div class="training-description">
						<p class="excerpt"><?php echo $excerpt ?></p>
					</div>
					<div class="training-info-data">
						<?php echo training_details($duration, $date, $location, $formatted_price) ?>
					</div>

				</div>
			</div>
        </section>
        <section class="training-content">
            <?php the_content(); ?>
		</section>
		<section class="training-signup">
			<div class="wrapper bg-green">
				<div class="container">
					<div class="row pt-m pb-m v-align-items-center">
						<div class="col-12 v-align-top align-center">
							<h2 class="has-text-align-center">Inschijven of meer informatie?</h2>
							<div class="training-cta-wrapper">
								<button class="read-more-button white js-open-modal" data-modal="#signup-modal" data-training="<?php echo $title ?> - <?php echo $date ?>">
									<span class="read-more-button-text"><?php echo SIGN_UP_TRAINING ?></span>
								</button>
							<?php if ($brochureID) { ?>
								<button class="read-more-button white js-open-modal" data-modal="#download-modal" data-training="<?php echo $ID ?>">
									<span class="read-more-button-text"><?php echo DOWNLOAD_PDF_TRAINING ?></span>
								</button>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>
</main>
<?php get_footer(); ?>