(function() {
	const selectors = {
		items: ".main-nav a[href^='#']",
		active: ".active"
	};

	const classes = {
		active: "active"
	};

	const menuLinks = Array.from(document.querySelectorAll(selectors.items));

	const animatedScroll = (event, top, left) => {
		event.preventDefault();

		window.scrollTo({
			top: top,
			left: left,
			behavior: "smooth"
		});
	};

	if (menuLinks) {
		menuLinks.forEach(link => {
			const href = link.getAttribute("href");

			// Get the ID from the Href and use it to get the element so we can calculate the offset
			const ID = href.substring(1);
			const elementToScrollTo = document.getElementById(ID);

			if (elementToScrollTo) {
				const top = elementToScrollTo.offsetTop;
				const left = elementToScrollTo.offsetLeft;
	
				link.addEventListener("click", (event) => {
					const anchor = event.currentTarget;
					const active = document.querySelector(selectors.active);

					// if the anchor includes current url then animated else not
					animatedScroll(event, top, left);
					
					if (active) {
						active.classList.remove(classes.active);
					}
					
					anchor.classList.add(classes.active);
				});
				// on page load check if current url has # and then animate scroll function 
			}
		});
	}

})();
