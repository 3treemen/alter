(function (){
    const selectors = {
        btnTop: ".btn-scroll-top"
    };

    const button = document.querySelector(selectors.btnTop);
    if (button){
        button.addEventListener("click", () => {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth"
            });
        });
    }
})();


