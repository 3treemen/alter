/*global jQuery*/
(function () {
	// const svgs = {
	// 	previous: "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDQ5MiA0OTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ5MiA0OTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxnPg0KCQk8cGF0aCBkPSJNMTk4LjYwOCwyNDYuMTA0TDM4Mi42NjQsNjIuMDRjNS4wNjgtNS4wNTYsNy44NTYtMTEuODE2LDcuODU2LTE5LjAyNGMwLTcuMjEyLTIuNzg4LTEzLjk2OC03Ljg1Ni0xOS4wMzJsLTE2LjEyOC0xNi4xMg0KCQkJQzM2MS40NzYsMi43OTIsMzU0LjcxMiwwLDM0Ny41MDQsMHMtMTMuOTY0LDIuNzkyLTE5LjAyOCw3Ljg2NEwxMDkuMzI4LDIyNy4wMDhjLTUuMDg0LDUuMDgtNy44NjgsMTEuODY4LTcuODQ4LDE5LjA4NA0KCQkJYy0wLjAyLDcuMjQ4LDIuNzYsMTQuMDI4LDcuODQ4LDE5LjExMmwyMTguOTQ0LDIxOC45MzJjNS4wNjQsNS4wNzIsMTEuODIsNy44NjQsMTkuMDMyLDcuODY0YzcuMjA4LDAsMTMuOTY0LTIuNzkyLDE5LjAzMi03Ljg2NA0KCQkJbDE2LjEyNC0xNi4xMmMxMC40OTItMTAuNDkyLDEwLjQ5Mi0yNy41NzIsMC0zOC4wNkwxOTguNjA4LDI0Ni4xMDR6Ii8+DQoJPC9nPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo=",
	// 	next: "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDQ5Mi4wMDQgNDkyLjAwNCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDkyLjAwNCA0OTIuMDA0OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Zz4NCgkJPHBhdGggZD0iTTM4Mi42NzgsMjI2LjgwNEwxNjMuNzMsNy44NkMxNTguNjY2LDIuNzkyLDE1MS45MDYsMCwxNDQuNjk4LDBzLTEzLjk2OCwyLjc5Mi0xOS4wMzIsNy44NmwtMTYuMTI0LDE2LjEyDQoJCQljLTEwLjQ5MiwxMC41MDQtMTAuNDkyLDI3LjU3NiwwLDM4LjA2NEwyOTMuMzk4LDI0NS45bC0xODQuMDYsMTg0LjA2Yy01LjA2NCw1LjA2OC03Ljg2LDExLjgyNC03Ljg2LDE5LjAyOA0KCQkJYzAsNy4yMTIsMi43OTYsMTMuOTY4LDcuODYsMTkuMDRsMTYuMTI0LDE2LjExNmM1LjA2OCw1LjA2OCwxMS44MjQsNy44NiwxOS4wMzIsNy44NnMxMy45NjgtMi43OTIsMTkuMDMyLTcuODZMMzgyLjY3OCwyNjUNCgkJCWM1LjA3Ni01LjA4NCw3Ljg2NC0xMS44NzIsNy44NDgtMTkuMDg4QzM5MC41NDIsMjM4LjY2OCwzODcuNzU0LDIzMS44ODQsMzgyLjY3OCwyMjYuODA0eiIvPg0KCTwvZz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K"
	// };

	const elements = {
		slider: ".slick-slider",
		slickInitialized: ".slick-initialized",
		prevButton: ".slick-button.previous",
		nextButton: ".slick-button.next",
		testimonialsBlock: ".testimonials-block",
		testimonialSlide: ".testimonial",
		trainingBlock: ".training-slider",
		trainingSlide: ".training-item"
	};

	// const navButtons = {
	// 	previousArrow: `<button type="button" class="slick-button previous"><img src=${svgs.previous} alt="Slider previous button"/></button>`,
	// 	nextArrow: `<button type="button" class="slick-button next"><img src=${svgs.next} alt="Slider previous button"/></button>`
	// };

	function init() {

		// Slider for the slider option on the row block
		jQuery(elements.slider).not(elements.slickInitialized).slick({
			// prevArrow: navButtons.previousArrow,
			// nextArrow: navButtons.nextArrow,
			appendArrows: false,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
			responsive: [
				{
					breakpoint: 768,
					settings: "unslick"
				}
			]
		});

		// Slider for references
		jQuery(".references-list").not(elements.slickInitialized).slick({
			// prevArrow: navButtons.previousArrow,
			// nextArrow: navButtons.nextArrow,
			appendArrows: false,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: false,
			slide: ".reference",
			mobileFirst: true,
			responsive: [
				{
					breakpoint: 992,
					settings: "unslick"
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				}
			]
		});

		// Slider for testimonials
		jQuery(elements.testimonialsBlock).not(elements.slickInitialized).slick({
			arrows: false,
			dots: false,
			autoplay: true,
			fade: true,
			speed: 2000,
			slidesToShow: 1,
			slidesToScroll: 1,
			slide: elements.testimonialSlide
		});

		// Slider for training
		

		if (jQuery(elements.trainingBlock).find(".training-item").length > 1){
			jQuery(elements.trainingBlock).not(elements.slickInitialized).slick({
				// prevArrow: navButtons.previousArrow,
				// nextArrow: navButtons.nextArrow,
				appendArrows: false,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: false,
				slide: elements.trainingSlide,
				mobileFirst: true,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
				]
			});
		}
	}

	jQuery(document).ready(function () {
		init();

		// //hide the previous button
		// jQuery(elements.prevButton).hide();

		// jQuery(elements.slider).on("afterChange", function(event, slick, currentSlide) {  	
		// 	//On the first slide hide the Previous button and show the Next
		// 	if (currentSlide === 0) {
		// 		jQuery(elements.prevButton).hide();
		// 		jQuery(elements.nextButton).show();
		// 	} else {
		// 		jQuery(elements.prevButton).show();
		// 	}

		// 	//On the last slide hide the Next button.
		// 	if (slick.slideCount === currentSlide + 1) {
		// 		jQuery(elements.nextButton).hide();
		// 	}
		// });
	});

	// Ugly way af loading the slider in the editor
	setTimeout(function () {
		init();
	}, 5000);

}());
