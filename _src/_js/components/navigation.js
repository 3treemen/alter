(function() {
    const classes = {
        active: "header-active",
        bodyLock: "has-overlay"
    };

    const selectors = {
        body: "body",
        header: ".js-header",
        navigationItem: ".menu-item",
        subNavigation: ".sub-menu",
        toggler: ".js-navigation-toggler",
        label: ".js-nav-label"
    };
    
    const body = document.querySelector(selectors.body);
    const header = document.querySelector(selectors.header);
    const toggler = document.querySelector(selectors.toggler);
    const navLabel = document.querySelector(selectors.label);


    if (toggler){
        // mobile navigation toggler
        toggler.addEventListener("click", () => {
            if (header.classList.contains(classes.active)) {
                header.classList.remove(classes.active);
                body.classList.remove(classes.bodyLock);
                navLabel.innerHTML = navLabel.getAttribute("open-close-label");
            } else {
                header.classList.add(classes.active);
                body.classList.add(classes.bodyLock);
                navLabel.innerHTML = navLabel.getAttribute("close-open-label");
            }
        });
    }
})();
