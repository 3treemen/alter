/*global $ */

(function () {
	
	const selectors = {
		form: ".js-modal-form",
		formWrapper : ".js-form-wrapper",
		formResponse : ".js-form-feedback"
	};
	
	const ajaxUrl = window.globals.ajax_url;
	const $form = $(selectors.form);

	if ($form.length > 0) {
		$form.on("submit", function (e) {
			e.preventDefault();
			$(this).addClass("submitted");
			if (!$(this)[0].reportValidity()){
				return;
			}

			let $formWrapper = $(this).find(selectors.formWrapper);
			let $formResponse = $(this).find(selectors.formResponse);

			const formValues = $(this).serialize() + "&action=send_training_email";
			var request = $.ajax({
				url: ajaxUrl,
				method: "POST",
				data: formValues
			});

			request.done(function(response) {
				if (response.success){
					$formWrapper.hide();
					$formResponse.html(response.data.message);
				}else{
					$formResponse.html("<p class='error'>Er is iets fout gegaan bij het versturen van uw aanvraag.</p>");
				}
			});

			request.fail(function() {
				$formResponse.html("<p class='error'>Er is iets fout gegaan bij het versturen van uw aanvraag.</p>");
			});
		});
	}

})();
