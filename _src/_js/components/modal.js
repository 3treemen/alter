(function() {

    const classes = {
        overlay: "has-overlay",
        show: "show"
    };

    const selectors = {
        closeModalBtn: ".modal-close-button",
        modal: ".js-modal",
		openModalBtn: ".js-open-modal"
	};
    
    const modals = document.querySelectorAll(selectors.modal);
    const openButtons = document.querySelectorAll(selectors.openModalBtn);
	const closeButtons = document.querySelectorAll(selectors.closeModalBtn);

    if (modals && modals.length > 0) {
        Array.from(modals).forEach(modal => {
            document.body.appendChild(modal);
        });
    }

    // add an event listener to open a modal
    if (openButtons && openButtons.length > 0) {
        Array.from(openButtons).forEach(trigger => {
            trigger.addEventListener("click", openModal);
        });
    }
    
    // add an event listener to close a modal
    if (closeButtons && closeButtons.length > 0) {
        Array.from(closeButtons).forEach(button => {
            button.addEventListener("click", closeModal);
        });   
    }

    function openModal(evt) {
		const button = evt.currentTarget;
		
        if (!("modal" in button.dataset)) {
			return;
        }
		
		const target = document.querySelector(button.dataset.modal);

        if (!target) {
			return;
		}
		
		const trainingType = button.dataset.training;
		const formField = target.querySelector("input[name='training']");

		if (trainingType) {
			formField.value = trainingType;
		}
		
        target.classList.add(classes.show);
        document.body.classList.add(classes.overlay);
    }

    function closeModal(evt) {
        const button = evt.currentTarget;
        button.closest(selectors.modal).classList.remove(classes.show);
		document.body.classList.remove(classes.overlay);
    }
}());
