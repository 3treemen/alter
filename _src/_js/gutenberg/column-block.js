const {registerBlockType, unregisterBlockType, getBlockTypes, getBlockAttributes} = wp.blocks
const {InnerBlocks, InspectorControls} = wp.blockEditor
const {PanelBody, SelectControl, ColorPalette, Button, ToggleControl} = wp.components
const {addFilter} = wp.hooks
const {createHigherOrderComponent} = wp.compose;

import {allowedBlocks} from "./helpers/column-allowed-blocks";

const addColClasses = createHigherOrderComponent( ( BlockListBlock ) => {
    return ( props ) => {
        return <BlockListBlock { ...props } className={props.block.attributes.colClass} />;
    };
}, 'customClassName' );

addFilter( 'editor.BlockListBlock', 'cw/column-block', addColClasses );


registerBlockType('cw/column-block', {
    // built in attributes
    title: "Kolom",
    description: "Voeg een kolom toe",
    icon: `layout`,
    category: `layout`,
    supports: {
        className: false
    },
    parent: [ 'cw/row-block' ],
    // custom attributes
    attributes: {
        colClass: {
            type: `string`
        },
        blockClass: {
            type: `sting`
        },
        bgColor: {
			type: `string`,
			default: ``
        },
        bgTransparant: {
			type: `boolean`,
			default: false
        },
        bgColorClass: {
			type: `string`,
			default: ``
        },
        hideOnMobile : {
            type: `boolean`,
            default : false
        },
        hideOnTablet : {
            type: `boolean`,
            default : false
        },
        hideOnDesktop : {
            type: `boolean`,
            default : false
        },
        verticalAlign : {
            type: `string`,
            default: `v-align-top`
        },
        blockMargin : {
            type: `string`,
            default : ``
        },
        topPadding : {
            type: `string`,
            default : ``
        },
        rightPadding : {
            type: `string`,
            default : ``
        },
        bottomPadding : {
            type: `string`,
            default : ``
        },
        leftPadding : {
            type: `string`,
            default : ``
        }
    },
    // render functions
    edit: props => {
        // destructuring from props
        const { setAttributes, className } = props
        const {colClass, blockClass, bgColor, bgTransparant, bgColorClass, verticalAlign, hideOnMobile, hideOnTablet, hideOnDesktop, blockMargin, topPadding, rightPadding, bottomPadding, leftPadding } = props.attributes
        const ALLOWED_BLOCKS = allowedBlocks;

        const verticalAlignOptions = [
            {label: `Boven`, value: `v-align-top`},
            {label: `Midden`, value: `v-align-center`},
            {label: `Onder`, value: `v-align-bottom`}
        ];

        const bgColorOptions = [
            { name: `Wit`, color: `#ffffff` },
            { name: `Grijs`, color: `#f2f2f2` },
            { name: `Licht Blauw`, color: `#148fad` },
            { name: `Medium Blauw`, color: `#1073ae` },
            { name: `Donker Blauw`, color: `#2d4a71` },
            { name: `Licht groen`, color: `#1dbfb1` },
            { name: `Donker groen`, color: `#21b5a2` }
        ];

        const blockMarginOptions = [
            {label: `Ja`, value: ``},
            {label: `Nee`, value: `true`}
        ];

        const topPaddingOptions = [
            {label: `Geen`, value: ``},
            {label: `Klein`, value: `pt-s`},
            {label: `Middel`, value: `pt-m`},
            {label: `Groot`, value: `pt-l`}
        ];

        const rightPaddingOptions = [
            {label: `Geen`, value: `pr-none`},
            {label: `Standaard`, value: ``},
            {label: `Klein`, value: `pr-s`},
            {label: `Middel`, value: `pr-m`},
            {label: `Groot`, value: `pr-l`}
        ];

        const bottomPaddingOptions = [
            {label: `Geen`, value: ``},
            {label: `Klein`, value: `pb-s`},
            {label: `Middel`, value: `pb-m`},
            {label: `Groot`, value: `pb-l`}
        ];

        const leftPaddingOptions = [
            {label: `Geen`, value: `pl-none`},
            {label: `Standaard`, value: ``},
            {label: `Klein`, value: `pl-s`},
            {label: `Middel`, value: `pl-m`},
            {label: `Groot`, value: `pl-l`}
        ];
        
        const bgColorChange = (color) => {
            if(!color) updateBlockClass('bgColorClass', '');
            if(color === `#ffffff`) updateBlockClass('bgColorClass', 'col-bg-white');
            if(color === `#f2f2f2`) updateBlockClass('bgColorClass', 'col-bg-grey');
            if(color === `#148fad`) updateBlockClass('bgColorClass', 'col-bg-blue-l');
            if(color === `#1073ae`) updateBlockClass('bgColorClass', 'col-bg-blue-m');
            if(color === `#2d4a71`) updateBlockClass('bgColorClass', 'col-bg-blue-d');
            if(color === `#1dbfb1`) updateBlockClass('bgColorClass', 'col-bg-green-l');
            if(color === `#21b5a2`) updateBlockClass('bgColorClass', 'col-bg-green');
            
            setAttributes({bgColor: color});
        };

        const updateBlockClass = (attr = null, value = null) => {
            if ( attr !== null ) {
                setAttributes({ [attr] : value});
            }

            let concatClass = colClass;
            concatClass += ` ${bgColorClass}`;
            concatClass += ` ${verticalAlign}`;
            concatClass += ` ${topPadding}`;
            concatClass += ` ${rightPadding}`;
            concatClass += ` ${bottomPadding}`;
            concatClass += ` ${leftPadding}`;
            concatClass += blockMargin ? ' col-margin' : '';
            concatClass += hideOnMobile ? ' hide-mobile' : '';
            concatClass += hideOnTablet ? ' hide-tablet' : '';
            concatClass += hideOnDesktop ? ' hide-desktop' : '';
            concatClass += bgTransparant ? ' bg-transp' : '';
            setAttributes({blockClass : concatClass.replace(/ +(?= )/g,'')});
        };

        updateBlockClass();

        return (
            <> 
                 <InspectorControls>
                    <PanelBody title="Uitlijning" initialOpen={false}>
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}><strong>Uitlijning verticaal:</strong></p>
                        <SelectControl
                            value={verticalAlign}
                            options={verticalAlignOptions}
                            onChange={ (value) => updateBlockClass('verticalAlign', value) }
                        />
                    </PanelBody>
                    <PanelBody title="Achtergrond" initialOpen={false}>
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}><strong>Achtergrondkleur kolom:</strong></p>
                        <ColorPalette
                            colors={bgColorOptions}
                            value={bgColor}
                            disableCustomColors={true}
                            onChange={bgColorChange}
                        />
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}></p>
                        <ToggleControl
                            label="Achtergrond transparant"
                            checked={ bgTransparant }
                            onChange={ (value) => updateBlockClass('bgTransparant', value) }
                        />
                    </PanelBody>
                    <PanelBody title="Witruimte" initialOpen={false}>
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}><strong>Kolommen aansluiten:</strong></p>
                        <SelectControl
                            value={blockMargin}
                            options={blockMarginOptions}
                            onChange={ (value) => updateBlockClass('blockMargin', value) }
                        />
                        <p className="inspector-control-label" style={{paddingTop: `20px`}}><strong>Witruimte boven:</strong></p>
                        <SelectControl
                            value={topPadding}
                            options={topPaddingOptions}
                            onChange={ (value) => updateBlockClass('topPadding', value) }
                        />
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}><strong>Witruimte rechts:</strong></p>
                        <SelectControl
                                value={rightPadding}
                                options={rightPaddingOptions}
                                onChange={ (value) => updateBlockClass('rightPadding', value) }
                        />
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}><strong>Witruimte onder:</strong></p>
                        <SelectControl
                                value={bottomPadding}
                                options={bottomPaddingOptions}
                                onChange={ (value) => updateBlockClass('bottomPadding', value) }
                        />
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}><strong>Witruimte links:</strong></p>
                        <SelectControl
                                value={leftPadding}
                                options={leftPaddingOptions}
                                onChange={ (value) => updateBlockClass('leftPadding', value) }
                        />
                    </PanelBody>
                    <PanelBody title="Zichtbaarheid" initialOpen={false}>
                        <p className="inspector-control-label" style={{paddingTop: `10px`}}>Let op! in de editor blijven de blokken zichtbaar.</p>
                        <ToggleControl
                            label="Verborgen op mobile"
                            checked={ hideOnMobile }
                            onChange={ (value) => updateBlockClass('hideOnMobile', value) }
                        />
                        <ToggleControl
                            label="Verborgen op tablet"
                            checked={ hideOnTablet }
                            onChange={ (value) => updateBlockClass('hideOnTablet', value) }
                        />
                        <ToggleControl
                            label="Verborgen op desktop"
                            checked={ hideOnDesktop }
                            onChange={ (value) => updateBlockClass('hideOnDesktop', value) }
                        />
                    </PanelBody>
                </InspectorControls>

                <div className={blockClass} >
                    <InnerBlocks
                        allowedBlocks={ALLOWED_BLOCKS} 
                        renderAppender={ () => (
                            <InnerBlocks.ButtonBlockAppender />
                        ) } 
                    />
                </div>
            </>
        )
    },
    save: props => {

        const { blockClass } = props.attributes

        return (
            <div className={blockClass}>
                <InnerBlocks.Content/>
            </div>
        )
    }
 
 });