const Button = (props) => {
	const client = "alterme."

    // Check if the URL includes site name with the '.' to account for external links with client in the query string
    const linkValue = props.linkValue && props.linkValue.toLowerCase().indexOf(client) !== -1 ? props.linkValue : `//${props.linkValue}`;
	const target = props.linkValue && props.linkValue.toLowerCase().indexOf(client) === -1;
	const relationship = target && "noopener noreferrer";
    return (
		<div class={`read-more-button-container ${props.alignment}`}>
            <a href={props.linkValue !== "" && props.linkValue !== undefined && linkValue} className={`read-more-button ${props.buttonTheme}`} target={target && "_blank"} rel={relationship}>
                <span className="read-more-button-text">{props.textContent} </span>
            </a>
		</div>
    );
};

export default Button;
