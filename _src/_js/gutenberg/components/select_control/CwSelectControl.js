import {SelectControl} from "@wordpress/components";

/**
 * Function to create select options for select control
 * @param {array} labels | Array of labels for the select control 
 * @param {array} values | Array of values for the select control
 */
const selectControlOptionCreator = (labels = [], values = []) => {
	if (labels.length !== values.length) {
		console.log("Limit Option Creator error, labels and values do not match");
		return;
	}

	const options = []
	for(let i = 0; i < labels.length; i++) {
		options.push({
			label: labels[i],
			value: values[i]
		})
	}
	return options;
}

const CwSelectControl = props => {
	
	return <SelectControl 
				label={props.label}
				value={props.value}
				options={props.options}
				onChange={props.onChange}
			/>
};

export {CwSelectControl, selectControlOptionCreator};