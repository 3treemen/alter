import {ColorPalette} from "@wordpress/components";

/**
 * Function to create a color class from hex value
 * @param {string} color | attribute for color for which a class needs to be generated
 * @param {*} attribute | The attribute which recieves the color class
 */

const setThemeClass = (color) => {
	let themeColorClass =  ``;
	switch (color) {
		case `#2d4a71`:
			themeColorClass = `theme-1`
			break;
		case `#1073ae`:
			themeColorClass = `theme-2`
			break;
		case `#148fad`:
			themeColorClass = `theme-3`
			break;
		case `#1dbfb1`:
			themeColorClass = `theme-4`
			break;
		case `#21b5a2`:
			themeColorClass = `theme-5`
			break;
		case `#262525`:
			themeColorClass = `grey-dark`
			break;
		case `#f2f2f2`:
			themeColorClass = `grey-medium`
			break;
		case `#eef1f3`:
			themeColorClass = `grey-light`
			break;
		case `#ffffff`:
			themeColorClass = `white`
			break;
		case `#000000`:
			themeColorClass = `black`
			break;
	};

	return themeColorClass;
}

const CwColorPalette = props => {

	return <ColorPalette 
				colors={props.colors}
				value={props.value}
				disableCustomColors={true}
				onChange={props.onChange}
			/>
};

CwColorPalette.defaultProps = {
	colors: [
		{label: "Thema 1", color: "#2d4a71"},
		{label: "Thema 2", color: "#1073ae"},
		{label: "Thema 3", color: "#148fad"},
		{label: "Thema 4", color: "#1dbfb1"},
		{label: "Thema 5", color: "#21b5a2"},
		{label: "Donker grijs", color: "#262525"},
		{label: "Medium grijs", color: "#f2f2f2"},
		{label: "Licht grijs", color: "#eef1f3"},
		{label: "Wit", color: "#ffffff"},
		{label: "Zwart", color: "#000000"}
	]
}

export {setThemeClass, CwColorPalette};