const ContactDetail = props => {
	const {type ,text, link, icon} = props;
	let linkType;

	if (type === "phone") {
		linkType = `tel:+${link}`;
	} else if (type === "email") {
		linkType = `mailto:${link}`;
	} else {
		linkType = link;
	}

	return (
		<>
			<li class="contact-block-link">
				<a  href={linkType}>
					{icon}
					<p>{text ? text : "Voer details in"}</p>
				</a>
			</li>
		</>
	)
}

export default ContactDetail;