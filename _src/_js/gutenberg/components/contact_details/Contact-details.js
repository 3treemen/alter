import icons from "../../helpers/contact-block-svgs";
import ContactDetail from "./Contact-detail";

const ContactDetails = props => {
	const {phone, email} = props 

	// The type prop is always the same as the type of contact informtion being passed to the component
	return (
		<>
			<ul class="contact-block-details">
				<ContactDetail
					type="phone"
					link={phone}
					text={phone}
					icon={icons.phone}
				/>
				<ContactDetail
					type="email"
					link={email}
					text={email}
					icon={icons.mail}
				/>
				<ContactDetail 
					type="linkedin"
					link="https://www.linkedin.com/company/28134465/"
					text="LinkedIn"
					icon={icons.linkedin}
				/>
			</ul>
		</>
	)
}

export default ContactDetails;