const {registerPlugin} = wp.plugins;
import {TextareaControl, ToggleControl, TextControl} from "@wordpress/components";
import { PluginDocumentSettingPanel } from '@wordpress/edit-post';
import {getMetaValues, setMetaValues} from "./helpers/utilities";

// Component to choose whether or not to show this testimonial in the slider
let ShowInBlockControl = props => {
	const {showInBlock, onMetaFieldChange} = props;

	return (
		<ToggleControl 
			label="Wil je deze testimonial in de slider laten zien?"
			checked={showInBlock}
			onChange={value => onMetaFieldChange(value)}
		/>
	);
};

// Component to enter testimonial text
let TestimonialContentControl = props => {
	const {testimonial, onMetaFieldChange} = props;

	return (
		<TextareaControl 
			label="Voer een testimonial in"
			value={testimonial}
			onChange={value => onMetaFieldChange(value)}
		/>
	);
};

// Component to enter testimonial author name
let TestimonialAuthorControl = props => {
	const {author, onMetaFieldChange} = props;

	return (
		<TextControl 
			label="Naam"
			value={author}
			onChange={value => onMetaFieldChange(value)}
		/>
	)
}

// Component to enter testimonial author name
let TestimonialAuthorCompanyControl = props => {
	const {authorCompany, onMetaFieldChange} = props;

	return (
		<TextControl 
			label="Bedrijf"
			value={authorCompany}
			onChange={value => onMetaFieldChange(value)}
		/>
	)
}

// Functions to get and set the value that determines if this block is shown or not
ShowInBlockControl = getMetaValues("showInBlock", "_show_testimonial_in_block")(ShowInBlockControl);
ShowInBlockControl = setMetaValues("_show_testimonial_in_block")(ShowInBlockControl);

// Functions to get and set the content of the testimonial
TestimonialContentControl = getMetaValues("testimonial", "_testimonial_content")(TestimonialContentControl);
TestimonialContentControl = setMetaValues("_testimonial_content")(TestimonialContentControl);

// Functions to get and set the author of the testimonial
TestimonialAuthorControl = getMetaValues("author", "_testimonial_author")(TestimonialAuthorControl);
TestimonialAuthorControl = setMetaValues("_testimonial_author")(TestimonialAuthorControl);

// Functions to get and set the author of the testimonial
TestimonialAuthorCompanyControl = getMetaValues("authorCompany", "_testimonial_author_company")(TestimonialAuthorCompanyControl);
TestimonialAuthorCompanyControl = setMetaValues("_testimonial_author_company")(TestimonialAuthorCompanyControl);

registerPlugin("testimonials-meta-sidebar", {
	icon: "",
	render: () => {

		return (
			<PluginDocumentSettingPanel name="testimonials-post-editor-panel" title="Blok instellingen">
				<ShowInBlockControl />
				<TestimonialContentControl />
				<TestimonialAuthorControl />
				<TestimonialAuthorCompanyControl />
			</PluginDocumentSettingPanel>
		);
	}
});
