/**
 * Dependencies
 */
import {registerBlockType} from "@wordpress/blocks";
import ServerSideRender from "@wordpress/server-side-render";

/**
 * Registering block on the client side
 */
registerBlockType("cw/testimonials-block", {
    //Built in attributes
    title: "Testimonial blok",
    description: "Een blok om testimonials weer te geven. Dit blok wordt weergegeven als een slider aan de voorkant van de site.",
    icon: "testimonial",
    category: "embed",
    supports: {
        className: false
    },
    //custom attributes
    attributes: {},
    
    //render functons
	edit: props => {
        return (
			<ServerSideRender block="cw/testimonials-block" attributes={props.attributes}/>
		)
    },
    save: props => {
        return null;
    }

})