/**
 * Dependencies
 */
const {registerBlockType} = wp.blocks;
const {InspectorControls} = wp.blockEditor;
const {PanelBody, TextControl} = wp.components;
const { serverSideRender: ServerSideRender } = wp;

import {CwSelectControl, selectControlOptionCreator} from "./components/select_control/CwSelectControl";

/**
 * Registering block on the client side
 */
registerBlockType("cw/blog-block", {
    //Built in attributes
    title: "Blog block",
    description: "Toon een blog overzicht",
    icon: "welcome-write-blog",
    category: "embed",
    supports: {
        className: false
    },
    //custom attributes
    attributes: {
        blockTitle: {
            type: `string`,
            default: ''
        },
        nrItems: {
            type: `string`,
            default: '1'
		},
		displayType: {
			type: "string",
			default: "horizontal"
		}
    },
    
    //render functons
	edit: props => {

        const { attributes, setAttributes } = props;
        const { blockTitle, nrItems, displayType } = attributes;

        const selectLimitOptions = selectControlOptionCreator(
			["1", "2", "3", "4", "5", "10", "Alle"],
			["1", "2", "3", "4", "5", "10", "-1"]
			);

		const displayOptions = selectControlOptionCreator(["Horizontal", "Vertical"], ["horizontal", "vertical"]);
		
        return (

            <>
            <InspectorControls>
                <PanelBody title="Opmaak blok" initialOpen={true}>
                    <TextControl
                        label="Blok titel"
                        value={ blockTitle }
                        onChange={blockTitle => setAttributes({blockTitle})}
                    />
                    <CwSelectControl 
                        label="Max aantal items"
                        value={nrItems}
                        options={selectLimitOptions}
                        onChange={nrItems => setAttributes({nrItems})}
                    />
					<CwSelectControl 
                        label="Hoe moet het worden weergegeven?"
                        value={displayType}
                        options={displayOptions}
                        onChange={displayType => setAttributes({displayType})}
                    />
                </PanelBody>
            
            </InspectorControls>

            <ServerSideRender
                block="cw/blog-block"
                attributes={ attributes }
            />

            </>
        )
    },
    save: props => {
        return null;
    }

})