import {registerBlockType} from "@wordpress/blocks";
import {InspectorControls} from "@wordpress/block-editor";
import {PanelBody} from "@wordpress/components";
import {useEffect} from "@wordpress/element";
import {CwColorPalette, setThemeClass} from "./components/color_palette/CwColorPalette";
import {CwSelectControl, selectControlOptionCreator} from "./components/select_control/CwSelectControl";
import icons from "./helpers/icon-block-svgs";

registerBlockType("cw/icon-block", {
	// Built in attributes
	title: "Icoon blok",
	description: "Selecteer een icoon",
	icon: "info-outline",
	category: "common",
	supports: {
		className: false
	},
	// Custom attributes
	attributes: {
		selectedIcon: {
			type: "string",
			default: ""
		},
		iconColor: {
			type: "string",
			default: "#ffffff"
		},
		iconClass: {
			type: "string"
		}
	},
	// render functions
	edit: props => {
		const {attributes, setAttributes} = props;
		const {selectedIcon, iconColor, iconClass} = attributes;

		// Providing different color options here cmpared to whats available by default in the CwColorPalette
		const colorOptions = [
			{label: "Blauw", color: "#2d4a71"},
			{label: "Wit", color: "#ffffff"}
		]

		// Setting the icon class based on the color selected
		useEffect(() => {
			setAttributes({iconClass: `svg-icon ${setThemeClass(iconColor)}`})
		}, [iconColor])

		// Creating options for CwSelectControl
		const iconSelectionOptions = selectControlOptionCreator(
			["Kies een optie","Invest", "Customers", "Eye", "Graph", "Heart", "Location", "Profile", "Data", "Plus"], 
			["kies een optie","invest", "customers", "eye", "graph", "heart", "location", "profile", "data", "plus"]
			)

		return (
			<>
				<InspectorControls>
					<PanelBody>
						<p className="inspector-control-label" style={{paddingTop: `10px`}}>Kies een kleur</p>
						<CwColorPalette 
                            colors={colorOptions}
                            value={iconColor}
                            disableCustomColors={true}
                            onChange={iconColor => setAttributes({iconColor})}
						/>
						<CwSelectControl 
							label="Selecteer een icoon"
							value={selectedIcon}
							options={iconSelectionOptions}
							onChange={selectedIcon => setAttributes({selectedIcon})}
						/>
					</PanelBody>
				</InspectorControls>

				<div className={iconClass}>
					{selectedIcon === "" || selectedIcon === "kies een optie" ? "Please select an icon" : icons && icons[selectedIcon]}
				</div>
			</>
		);
	},
	save: props => {
		const {selectedIcon, iconClass} = props.attributes;
		
		return (
			<div className={iconClass}>
				{icons && icons[selectedIcon]}
			</div>
		);
	}
})