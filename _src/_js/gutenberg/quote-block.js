/**
 * Dependencies
 */
const {registerBlockType} = wp.blocks;
const {InspectorControls, URLInput, RichText, BlockControls, AlignmentToolbar} = wp.blockEditor;
const {PanelBody, TextControl, SelectControl, ColorPalette} = wp.components;

/**
 * Registering block on the client side
 */
registerBlockType("cw/quote-block", {
    //Built in attributes
    title: "Quote",
    description: "Een quote element",
    icon: "editor-quote",
    category: "common",
    supports: {
        className: false
    },
    //custom attributes
    attributes: {
        quoteText: {
            type: `string`,
            default: ``
        },
        quoteAuthor: {
            type: `string`,
            default: ``
        },
        quoteAlignment: {
            type: `string`,
            default: `align-left`
        },
        color: {
            type: `string`,
            default: `align-left`
        }
    },
    
    //render functons
    edit: props => {
        // destructuring props
        const { attributes, setAttributes } = props
        const { quoteText, quoteAlignment, color} = attributes

        return (
            <>  
                <BlockControls>
                    <AlignmentToolbar
                        value={quoteAlignment}
                        onChange={(quoteAlignment) => setAttributes({ quoteAlignment })}
                    />
                </BlockControls>
                <blockquote className={ quoteAlignment }>
                    <RichText
                        tagName="span"
                        value= { quoteText }
                        onChange={ ( quoteText ) => setAttributes( { quoteText } ) }
                        placeholder = "Quote"
                        keepPlaceholderOnFocus = { true }
                        multiline= {false}
                    />
                </blockquote>
            </>
        )
    },
    save: props => {

        const { quoteText, quoteAlignment } = props.attributes

        return (
            <blockquote className={ quoteAlignment }>
                <RichText.Content tagName="span" value={ quoteText } />
            </blockquote>
        )
    }

})