import {registerBlockType} from "@wordpress/blocks";
import {RichText, InspectorControls} from '@wordpress/block-editor';
import {PanelBody, ColorPalette} from "@wordpress/components";

registerBlockType("cw/subtitle-block", {
	title: "Subtitel",
	description: "Maak een ondertitel",
	icon: "edit",
	category: "common",
	supports: {
		className: false
	},
	attributes: {
		subtitle: {
			type: "string"
		},
		textColor: {
			type: "string",
			default: "#0b0b0b"
		},
		colorClass: {
			type: "string"
		}
	},
	//render functions
	edit: props => {
		const {attributes, setAttributes} = props;
		const {subtitle, textColor, colorClass} = attributes;

		const colorOptions = [
            {label: "Groen", color: "#00b279"},
            {label: "Blauw", color: "#00dcda"},
            {label: "Licht groen", color: "#98ff06"},
            {label: "Rood", color: "#f23f2a"},
            {label: "Roze", color: "#ffabd1"},
			{label: "Geel", color: "#ffce00"},
			{label: "Wit", color: "#ffffff"},
			{label: "Zwart", color: "#0b0b0b"}
		]
		
		const setColorClass = () => {
            let concatClass =  ``;
			switch (textColor) {
				case `#00b279`:
					concatClass = `theme-2`
					break;
				case `#00dcda`:
					concatClass = `theme-3`
					break;
				case `#98ff06`:
					concatClass = `theme-4`
					break;
				case `#f23f2a`:
					concatClass = `theme-5`
					break;
				case `#ffabd1`:
					concatClass = `theme-5`
					break;
				case `#ffce00`:
					concatClass = `theme-6`
					break;
				case `#ffffff`:
					concatClass = `white`
					break;
				case `#0b0b0b`:
					concatClass = `primary`
					break;
			}
            setAttributes({colorClass : concatClass.replace(/ +(?= )/g,'')});
        };
		setColorClass();
		
		return (
			<>
				<InspectorControls>
					<PanelBody
						title="Kleurinstellingen"
						initialOpen={true}
					>
						<ColorPalette 
							colors={colorOptions}
                            value={textColor}
                            disableCustomColors={true}
                            onChange={textColor => setAttributes({textColor})}
						/>

					</PanelBody>
				</InspectorControls>
				<RichText 
					tagName="span"
					value={subtitle}
					className={`subtitle-block ${colorClass}`}
					placeholder="Ondertiteling"
					keepPlaceholderOnFocus={true}
					onChange={subtitle => setAttributes({subtitle})}
				/>
			</>
		)
	},
	save: props => {
		const {attributes: {subtitle, colorClass}} = props;

		return <RichText.Content tagName="span" value={subtitle} className={`subtitle-block ${colorClass}`}/>
	}
})