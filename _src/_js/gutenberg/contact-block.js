import {registerBlockType} from "@wordpress/blocks";
import {InspectorControls, RichText} from "@wordpress/block-editor";
import {PanelBody, TextControl} from "@wordpress/components";

import ContactDetails from "./components/contact_details/Contact-details";


registerBlockType("cw/contact-block", {
	title: "Contact",
	description: "Een blok voor contactgegevens",
	icon: "email",
	category: "common",
	supports: {
		className: false
	},
	attributes: {
		blockTitle: {
			type: "String"
		},
		blockSubtitle: {
			type: "string"
		},
		blockText: {
			type: "string"
		},
		phoneNumber: {
			type: "string"
		},
		email: {
			type: "string"
		}
	},
	edit: props => {
		const {attributes, setAttributes} = props;
		const {blockTitle, blockSubtitle, blockText, phoneNumber, email} = attributes;

		return (
			<>	
				<InspectorControls>
					<PanelBody initialOpen={true} title="Contact details">
						<TextControl 
							label="Telefoonnummer"
							value={phoneNumber}
							onChange={phoneNumber => setAttributes({phoneNumber})}
						/>
						<TextControl 
							label="E-mailadres"
							value={email}
							onChange={email => setAttributes({email})}
						/>
					</PanelBody>				
				</InspectorControls>
				<div class="contact-block">
					<div class="contact-block-information">
						<RichText 
							tagName="h2"
							value={blockTitle}
							className="information-title"
							placeholder="Voer wat tekst in"
							keepPlaceholderOnFocus={true}
							onChange={blockTitle => setAttributes({blockTitle})}
						/>
						<RichText 
							tagName="h3"
							value={blockSubtitle}
							className="information-subtitle"
							placeholder="Voer wat tekst in"
							keepPlaceholderOnFocus={true}
							onChange={blockSubtitle => setAttributes({blockSubtitle})}
						/>
						<RichText 
							tagName="p"
							value={blockText}
							className="information-text"
							placeholder="Voer wat tekst in"
							keepPlaceholderOnFocus={true}
							onChange={blockText => setAttributes({blockText})}
						/>
					</div>
					<ContactDetails 
						phone={phoneNumber}
						email={email}
					/>
				</div>
			</>
		)
	},
	save: props => {
		const {attributes: {blockTitle, blockSubtitle, blockText, phoneNumber, email}} = props;

		return (
			<>
			<div class="contact-block">
				<div class="contact-block-information">
					<RichText.Content 
						tagName="h2" 
						value={blockTitle} 
						className="information-title"
					/>
					<RichText.Content 
						tagName="h3" 
						value={blockSubtitle} 
						className="information-subtitle"
					/>
					<RichText.Content 
						tagName="p" 
						value={blockText} 
						className="information-text"
					/>
				</div>
				<ContactDetails 
					phone={phoneNumber}
					email={email}
				/>
			</div>
			</>
		)
	}
})