/**
 * Dependencies
 */
const {registerBlockType} = wp.blocks;
const {InspectorControls} = wp.blockEditor;
const {PanelBody, TextControl} = wp.components;
const {serverSideRender: ServerSideRender} = wp

/**
 * Registering block on the client side
 */
registerBlockType("cw/references-block", {
    //Built in attributes
    title: "References block",
    description: "A block to display the references posts",
    icon: "format-gallery",
    category: "embed",
    supports: {
        className: false
    },
    //custom attributes
    attributes: {
        blockTitle: {
            type: `string`,
            default: ''
        }
    },
    
    //render functons
	edit: props => {

        const { attributes, setAttributes } = props;
        const { blockTitle} = attributes;

        return (

            <>
				<InspectorControls>
					<PanelBody title="Block settings" initialOpen={true}>
						<TextControl
							label="Block title"
							value={blockTitle}
							onChange={blockTitle => setAttributes({blockTitle })}
						/>
					</PanelBody>           
				</InspectorControls>

				<ServerSideRender block="cw/references-block" attributes={attributes}/>
            </>
        )
    },
    save: props => {
        return null;
    }

})