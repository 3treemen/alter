/**
 * Dependencies
 */
const {registerBlockType} = wp.blocks;
const {InspectorControls, URLInput} = wp.blockEditor;
const {PanelBody, TextControl, SelectControl, ColorPalette} = wp.components;
import {useEffect} from "@wordpress/element";

import Button from "./components/button/Button"
import {setThemeClass, CwColorPalette} from "./components/color_palette/CwColorPalette";
import {CwSelectControl, selectControlOptionCreator} from "./components/select_control/CwSelectControl";

/**
 * Registering block on the client side
 */
registerBlockType("cw/button-block", {
    //Built in attributes
    title: "Knop",
    description: "Een generieke knop om op de hele site te gebruiken",
    icon: "image-filter",
    category: "common",
    supports: {
        className: false
    },
    //custom attributes
    attributes: {
        buttonText: {
            type: "string",
            default: "Lees meer"
        },
        link: {
            type: "string"
        },
        buttonTheme: {
            type: "string",
            default: "theme-1"
        },
        buttonColor: {
            type: "string",
            default: "#2d4a71"
        },
        alignment: {
            type: "string",
            default: "align-left"
        }
    },
    
    //render functons
    edit: props => {
        // destructuring props
        const { attributes, setAttributes } = props
        const { buttonText, link, buttonType, buttonTheme, buttonColor, alignment } = attributes;
        
        // Options for button alignment select control
		const alignmentOptions = selectControlOptionCreator(
			["Links", "Midden", "Recths"],
			["align-left", "align-center", "align-right"]
		)
        
        useEffect(() => {
			setAttributes({buttonTheme: setThemeClass(buttonColor)})
		}, [buttonColor])

        return (
            <>
                <InspectorControls>
                    <PanelBody
                        title="Knop tekst"
                        initialOpen={true}
                    >
                        <TextControl 
                            label="Voer een knop tekst in"
                            value={buttonText}
                            onChange={buttonText => setAttributes({buttonText})}
                        />
                    </PanelBody>
                    <PanelBody
                        title="Link instellingen"
                        initialOpen={true}
                    >
                        <URLInput 
                            label="Voer een link in of zoek naar een pagina"
                            value={link}
                            onChange={link => setAttributes({link})}
                            className="link-picker"
                        />
                    </PanelBody>
                    <PanelBody
                        title="Knop kleur"
                        initialOpen={false}
                    >
                        <CwColorPalette 
                            value={buttonColor}
                            onChange={buttonColor => setAttributes({buttonColor})}
                        />
                    </PanelBody>
                    <PanelBody
                        title="Uitlijning"
                        initialOpen={false}
                    >
                        <CwSelectControl 
                            label="Kies een uitlijningsoptie"
                            value={alignment}
                            options={alignmentOptions}
                            onChange={alignment => setAttributes({alignment})}
                        />
                    </PanelBody>
                </InspectorControls>
                <Button 
                    textContent={buttonText}
                    linkValue={link}
                    buttonTheme={buttonTheme}
                    alignment={alignment}
                />
            </>
        )
    },
    save: props => {

        const { buttonText, link, buttonTheme, alignment } = props.attributes

        return (
            <Button 
                textContent={buttonText}
                linkValue={link}
                buttonTheme={buttonTheme}
                alignment={alignment}
            />
        )
    }

})