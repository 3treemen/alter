const {registerBlockType} = wp.blocks
const {InnerBlocks, InspectorControls, MediaUpload} = wp.blockEditor
const {PanelBody, SelectControl, ColorPalette, Button, FocalPointPicker, Placeholder, ToggleControl, TextControl} = wp.components
const {addFilter} = wp.hooks
import variations from "./helpers/column-variations";

function createBgImageStyle(attributes){
    const {bgImage, bgImageFocalPoint, bgImageSize} = attributes

    return {
        backgroundImage: bgImage && `url(${bgImage})`,
        backgroundSize: bgImage && bgImageSize,
        backgroundPosition: bgImage && `${bgImageFocalPoint.x * 100}% ${bgImageFocalPoint.y * 100}%`,
        backgroundRepeat: bgImage && `no-repeat`
    };
}

registerBlockType('cw/row-block', {
    // built in attributes
    title: "Rij",
    description: "Voeg een rij toe",
    icon: `align-center`,
    category: `layout`,
    supports: {
        className: false
    },
    // custom attributes
    attributes: {
        wrapperClass: {
            type: `string`,
            default: `wrapper`,
        },
        wrapperBgColor: {
            type: `string`,
			default: ``
        },
        rowClass: {
            type: `string`,
            default: `row`
        },
        rowPosition: {
            type: `string`,
            default: `center`
        },
        topPadding : {
            type: `string`,
            default : `pt-s`
        },
        bottomPadding : {
            type: `string`,
            default : `pb-s`
        },
        hideOnMobile : {
            type: `boolean`,
            default : false
        },
        hideOnTablet : {
            type: `boolean`,
            default : false
        },
        hideOnDesktop : {
            type: `boolean`,
            default : false
        },
        bgImage: {
            type: `string`,
            deafult: null
        },
        bgImagePosition: {
            type: `string`,
            default: `center`
        },
        bgImageSize: {
            type: `string`,
            default: `cover`
        },
        bgImageFocalPoint: {
            type: 'object',
            default: {
                x: 0,
                y: 0
            }
        },
        selectedVariation: {
		},
		rowID: {
			type: "string"
		},
		sliderOnMobile: {
			type: "boolean",
			default: false
		}
    },

    // render functions
    edit: props => {
        // destructuring from props
        const { setAttributes, attributes } = props
        const { 
			wrapperClass, 
			wrapperBgColor, 
			rowClass, 
			topPadding, 
			bottomPadding, 
			hideOnMobile, 
			hideOnTablet, 
			hideOnDesktop, 
			bgImage, 
			bgImageFocalPoint, 
			bgImageSize, 
			selectedVariation,
			rowID,
			sliderOnMobile } = attributes;
        
        const wrapperBackgroundColors = [
            { name: `Grijs`, color: `#f2f2f2` },
            { name: `Licht Blauw`, color: `#148fad` },
            { name: `Medium Blauw`, color: `#1073ae` },
            { name: `Donker Blauw`, color: `#2d4a71` },
            { name: `Licht groen`, color: `#1dbfb1` },
            { name: `Donker groen`, color: `#21b5a2` }
        ];

        const topPaddingOptions = [
            {label: `Geen`, value: ``},
            {label: `Klein`, value: `pt-s`},
            {label: `Middel`, value: `pt-m`},
            {label: `Groot`, value: `pt-l`}
        ];

        const bottomPaddingOptions = [
            {label: `Geen`, value: ``},
            {label: `Klein`, value: `pb-s`},
            {label: `Middel`, value: `pb-m`},
            {label: `Groot`, value: `pb-l`}
        ];

        const bgImageSizeOptions = [
            {label: `Bedekken`, value: `cover`},
            {label: `Vashouden`, value: `contain`},
            {label: `Auto`, value: `auto`}
        ];

        const setWrapperClass = (color) => {
            let concatClass =  `wrapper`;
            if(wrapperBgColor === `#f2f2f2`) concatClass += ` bg-grey`;
            if(wrapperBgColor === `#148fad`) concatClass += ` bg-blue-l`;
            if(wrapperBgColor === `#1073ae`) concatClass += ` bg-blue-m`;
            if(wrapperBgColor === `#2d4a71`) concatClass += ` bg-blue-d`;
            if(wrapperBgColor === `#1dbfb1`) concatClass += ` bg-green-l`;
            if(wrapperBgColor === `#21b5a2`) concatClass += ` bg-green`;

            concatClass += hideOnMobile ? ` hide-mobile` : ``;
            concatClass += hideOnTablet ? ` hide-tablet` : ``;
            concatClass += hideOnDesktop ? ` hide-desktop` : ``;
            setAttributes({wrapperClass : concatClass.replace(/ +(?= )/g,'')});
        };
        setWrapperClass();

        const setRowClass = () => {
            let concatClass = `row`;
            concatClass += ` ${topPadding}`;
			concatClass += ` ${bottomPadding}`;
			concatClass += sliderOnMobile ? ` slick-slider` : ``;
            setAttributes({rowClass : concatClass.replace(/ +(?= )/g,'')});
        };
		setRowClass();

        const wrapperStyle = createBgImageStyle(attributes);

        const handleBgImageRender = ({open}) => {
            return (
                <Button onClick={open} icon="upload">
                    Kies een afbeelding
                </Button>
            );
		};

        return (
            <>
                <InspectorControls>
                    <PanelBody title="Witruimte" initialOpen={false}>
                    <p className="inspector-control-label"><strong>Witruimte boven:</strong></p>
                    <SelectControl
                        value={topPadding}
                        options={topPaddingOptions}
                        onChange={ (value) => setAttributes({topPadding:value}) }
                    />
                    <p className="inspector-control-label"><strong>Witruimte onder:</strong></p>
                    <SelectControl
                        value={bottomPadding}
                        options={bottomPaddingOptions}
                        onChange={ (value) => setAttributes({bottomPadding:value}) }
                    />
                    </PanelBody>
                    <PanelBody title="Achtergrond kleur" initialOpen={false} >
                        <p className="inspector-control-label"><strong>Kies een kleur:</strong></p>
                        <ColorPalette
                            colors={wrapperBackgroundColors}
                            value={wrapperBgColor}
                            disableCustomColors={true}
                            onChange={ (value) => setAttributes({wrapperBgColor:value}) }
                        />
                    </PanelBody>
                    <PanelBody title="Achtergrond afbeelding" initialOpen={false}>
                        <p className="inspector-control-label"><strong>Selecteer een afbeelding:</strong></p>
                        <MediaUpload
                            value={bgImage}
                            onSelect={(newImg) => setAttributes({bgImage: newImg.sizes.full.url})}
                            type="image"
                            render={handleBgImageRender}
                        />
                        <p className="inspector-control-label"><strong>Positie afbeelding:</strong></p>
                        <FocalPointPicker 
                            url={bgImage ? bgImage : `https://picsum.photos/200`}
                            value={bgImageFocalPoint}
                            onChange={(focusPoint) => setAttributes({bgImageFocalPoint:focusPoint})}
                        />
                        <p className="inspector-control-label"><strong>Formaat afbeelding:</strong></p>
                        <SelectControl
                            value={bgImageSize}
                            options={bgImageSizeOptions}
                            onChange={ (value) => setAttributes({bgImageSize:value} ) }
                        />
                    </PanelBody>
                    <PanelBody title="Zichtbaarheid" initialOpen={false}>
                        <p className="inspector-control-label">Let op! in de editor blijven de blokken zichtbaar.</p>
                        <ToggleControl
                            label="Verborgen op mobile"
                            checked={ hideOnMobile }
                            onChange={ (value) => setAttributes({hideOnMobile:value} ) }
                        />
                        <ToggleControl
                            label="Verborgen op tablet"
                            checked={ hideOnTablet }
                            onChange={ (value) => setAttributes({hideOnTablet:value} ) }
                        />
                        <ToggleControl
                            label="Verborgen op desktop"
                            checked={ hideOnDesktop }
                            onChange={ (value) => setAttributes({hideOnDesktop:value} ) }
                        />
                    </PanelBody>
					<PanelBody title="Rij-ID" initialOpen={false}>
						<TextControl 
							label="Voer een ID in voor deze rij"
							value={rowID}
							onChange={rowID => setAttributes({rowID: rowID.toLowerCase()})}
						/>
					</PanelBody>
					<PanelBody title="Schuifregelaar" initialOpen={false}>
					<p className="inspector-control-label">Let op! Je ziet de schuifregelaar alleen op mobiel, niet in de editor.</p>
						<ToggleControl 
							label="Rij weergeven als schuifregelaar op mobiel?"
							checked={sliderOnMobile}
							onChange={sliderOnMobile => setAttributes({sliderOnMobile})}
						/>
					</PanelBody>
                </InspectorControls>

                {
                    attributes.selectedVariation ? 

                    <div id={rowID && rowID} className={`${wrapperClass}`} style={wrapperStyle} >
                        <div className="container">
                            <div className={`${rowClass}`}>
                                <InnerBlocks 
                                    allowedBlocks={''}
                                    template={attributes.selectedVariation.innerBlocks}
                                    renderAppender={false}
                                />
                            </div>
                        </div>
                    </div>

                    :

                    <Placeholder icon='' label='Kies een kolomstructuur' instructions='' >
                        <ul className="block-editor-block-variation-picker__variations" role="list" >
                            { variations.map( ( variation ) => (
                                <li key={ variation.name }>
                                    <Button
                                        isSecondary
                                        icon={ variation.icon }
                                        iconSize={ 48 }
                                        onClick={ () => setAttributes({selectedVariation:variation}) }
                                        className="block-editor-block-variation-picker__variation"
                                        label={ variation.description || variation.title }
                                    />
                                    <span className="block-editor-block-variation-picker__variation-label" role="presentation">
                                        { variation.title }
                                    </span>
                                </li>
                            ) ) }
                        </ul>
                    </Placeholder>
                }
            </>
        )
    },
    save: props => {
        const { attributes } = props
        const { wrapperClass, rowClass, rowID} = attributes
        const wrapperStyle = createBgImageStyle(attributes);

        return (
            <div id={rowID && rowID} className={`${wrapperClass}`} style={wrapperStyle}>
                <div className="container">
                    <div className={`${rowClass}`}>
                        <InnerBlocks.Content/>
                    </div>
                </div>
            </div>
        )
    }
 
 });