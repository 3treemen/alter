const {registerPlugin} = wp.plugins;
import {TextControl, ColorPalette, SelectControl, TextareaControl} from "@wordpress/components";
import {withSelect, withDispatch} from "@wordpress/data";
import {PluginDocumentSettingPanel} from '@wordpress/edit-post';
import {getMetaValues, setMetaValues} from "./helpers/utilities";

let ReferencesShowInBlock = (props) => {
    const selectOptions = [
        {label: "Selecteer een optie", value: ""},
        {label: "Ja", value: "true"},
        {label: "Nee", value: "false"}
    ]

    return (
        <>
            <SelectControl 
                label="Toon in block"
                value={props._referrer_show_in_block}
                options={selectOptions}
                onChange={(value) => props.onMetaFieldChange(value)}
            />
        </>
    )
}

ReferencesShowInBlock = withSelect(
    (select) => {
        return {
            show_in_block: select('core/editor').getEditedPostAttribute('meta')['_referrer_show_in_block']
        }
    }
)(ReferencesShowInBlock);

ReferencesShowInBlock = withDispatch(
    (dispatch) => {
        return {
            onMetaFieldChange: (value) => {
                dispatch('core/editor').editPost({meta: {_referrer_show_in_block: value}})
            }
        }
    }
)(ReferencesShowInBlock);

let ReferenceBlockBackgroundColor = (props) => {
	const {block_background_color, onMetaFieldChange} = props;

    const bgColorOptions = [
        { name: `Licht Blauw`, color: `#148fad` },
        { name: `Medium Blauw`, color: `#1073ae` },
        { name: `Donker Blauw`, color: `#2d4a71` },
        { name: `Licht groen`, color: `#1dbfb1` },
        { name: `Donker groen`, color: `#21b5a2` }
    ];

    return (
        <ColorPalette
            colors={bgColorOptions}
            value={block_background_color}
            disableCustomColors={true}
            onChange={value => onMetaFieldChange(value)}
        />
    );
};

ReferenceBlockBackgroundColor = getMetaValues("block_background_color", "_referrer_block_background_color")(ReferenceBlockBackgroundColor);
ReferenceBlockBackgroundColor = setMetaValues("_referrer_block_background_color")(ReferenceBlockBackgroundColor);

let ReferencesReferrerName = (props) => {
	const {referrerName, onMetaFieldChange} = props;

    return (
		<TextControl 
			label="Naam"
			value={referrerName}
			onChange={value => onMetaFieldChange(value)}
		/>
    );
};

ReferencesReferrerName = getMetaValues("referrerName", "_referrer_name")(ReferencesReferrerName);
ReferencesReferrerName = setMetaValues("_referrer_name")(ReferencesReferrerName);

let ReferencesReferrerJobTitle = (props) => {
	const {referrerJobTitle, onMetaFieldChange} = props;

    return (
        <TextControl 
			label="Functie"
			value={referrerJobTitle}
			onChange={value => onMetaFieldChange(value)}
		/>
    )
}

ReferencesReferrerJobTitle = getMetaValues("referrerJobTitle", "_referrer_job_title")(ReferencesReferrerJobTitle);
ReferencesReferrerJobTitle = setMetaValues("_referrer_job_title")(ReferencesReferrerJobTitle);

let ReferencesReferrerQuote = (props) => {
	const {referrerQuote, onMetaFieldChange} = props;

    return (
		<TextareaControl
			label="Quote"
			value={referrerQuote}
			onChange={value => onMetaFieldChange(value)}
		/>
    );
};

ReferencesReferrerQuote = getMetaValues("referrerQuote", "_referrer_quote")(ReferencesReferrerQuote);
ReferencesReferrerQuote = setMetaValues("_referrer_quote")(ReferencesReferrerQuote);

registerPlugin( 'references-meta-sidebar', {
    icon: '',
    render: () => {
        return (
            <>
                <PluginDocumentSettingPanel
                    name="references-displau-editor-panel"
                    title="Blok instellingen">
                    <ReferencesShowInBlock />
                    <p className="inspector-control-label" style={{paddingTop: `10px`}}><strong>Kleur achtergrond:</strong></p>
                    <ReferenceBlockBackgroundColor />
                </PluginDocumentSettingPanel>
                <PluginDocumentSettingPanel
                    name="references-content-editor-panel"
                    title="Referentie gegevens">
                    <ReferencesReferrerName />
                    <ReferencesReferrerJobTitle />
					<ReferencesReferrerQuote />
                </PluginDocumentSettingPanel>
            </>
        )
    }
})
