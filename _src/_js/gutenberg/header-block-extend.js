
const { addFilter } = wp.hooks
const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, SelectControl } = wp.components;


const addHeaderStyleControlAttribute = ( settings, name ) => {
    if ( name !== 'core/heading') {
        return settings;
    }

    settings.attributes.className = {
        type: 'string'
    }

    return settings;
};

addFilter( 'blocks.registerBlockType', 'cw/header-block-extend', addHeaderStyleControlAttribute );


const headerStylesControls = createHigherOrderComponent( ( BlockEdit ) => {
    return ( props ) => {

        if ( props.name !== 'core/heading') {
            return (
                <BlockEdit { ...props } />
            );
        }
        const { setAttributes } = props
        const { className } = props.attributes;

        const headerStyles = [
            {label: "Standaard", value: ""},
            {label: "H1", value: "header1"},
			{label: "H2", value: "header2"},
            {label: "H3", value: "header3"},
            {label: "H4", value: "header4"},
            {label: "H5", value: "header5"},
            {label: "H6", value: "header6"}
        ]

        return (
            <Fragment>
                <BlockEdit { ...props }/>
                <InspectorControls>
                    <PanelBody
                        title={"Weergave"}
                    >
                    <SelectControl 
                        label="Style"
                        value={className}
                        options={headerStyles}
                        onChange={className => setAttributes({className})}
                    />
                    </PanelBody>
                </InspectorControls>
            </Fragment>
        );
    };
}, 'withSpacingControl' );

addFilter( 'editor.BlockEdit', 'cw/header-block-extend', headerStylesControls );
