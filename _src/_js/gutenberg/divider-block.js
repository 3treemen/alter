/**
 * Dependencies
 */
import {registerBlockType} from "@wordpress/blocks";
import {InspectorControls} from "@wordpress/block-editor";
import {PanelBody} from "@wordpress/components";
import {CwSelectControl, selectControlOptionCreator} from "./components/select_control/CwSelectControl";

/**
 * Registering block on the client side
 */
registerBlockType("cw/divider-block", {
    //Built in attributes
    title: "Divider",
    description: "Een divider element",
    icon: "minus",
    category: "common",
    supports: {
        className: false
    },
	// Custom attributes
	attributes: {
		selectedWidth: {
			type: "string",
			default: ""
        }
	},

    //render functons
    edit: props => {
        const {attributes, setAttributes} = props;
        const {selectedWidth} = attributes;
        // Creating options for CwSelectControl
		const widthSelectionOptions = selectControlOptionCreator(
            ["Small","Medium", "Large"], 
            ["divider-small","divider-medium", "divider-large"]
            )
            const handleWidthChange = selectedWidth => {
                setAttributes({selectedWidth});
            }
		return (
            <>
				<InspectorControls>
					<PanelBody>
						<CwSelectControl 
							label="Selecteer de breedte"
							value={selectedWidth}
							options={widthSelectionOptions}
						onChange={handleWidthChange}
						/>
					</PanelBody>
				</InspectorControls>
                <div class={`divider-block ${selectedWidth}`}></div>
            </>
        )

    },
    save: props => {
        const {attributes} = props;
        const {selectedWidth} = attributes;
        return (
            <>
                <div class={`divider-block ${selectedWidth}`}></div>
            </>
        );
    }

})