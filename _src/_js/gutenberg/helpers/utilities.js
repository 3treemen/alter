import {withSelect, withDispatch} from "@wordpress/data";

const getMetaValues = (metaLabel, metaKey) => {
	return withSelect(
		select => {
			return {
				[metaLabel]: select("core/editor").getEditedPostAttribute("meta")[metaKey]
			};
		}
	);
};

const setMetaValues = (metaKey) => {
	return withDispatch(
		dispatch => {
			return {
				onMetaFieldChange: (value) => dispatch("core/editor").editPost({meta: {[metaKey]: value}})
			};
		}
	);
};

export {getMetaValues, setMetaValues};
