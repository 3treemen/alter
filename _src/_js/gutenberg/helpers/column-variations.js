import {SVG, Path} from "@wordpress/components";

const variations = [
     {
        name: "one-column",
        title: "1 kolom: 100%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-12"}]]
    },
    {
        name: "two-columns-fifty-fifty",
        title: "2 kolommen: 50% - 50%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="30" y1="15" x2="30" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-6"}], ['cw/column-block', {colClass: "col-6"}]]
    },
    {
        name: "two-columns-one-third-two-third",
        title: "2 kolommen: 33% - 66%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="18" y1="15" x2="18" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-4"}], ['cw/column-block', {colClass: "col-8"}]]
    },
    {
        name: "two-columns-two-third-one-third",
        title: "2 kolommen: 66% - 33%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="41" y1="15" x2="41" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-8"}], ['cw/column-block', {colClass: "col-4"}]]
    },
    {
        name: "three-column-equal",
        title: "3 kolommen: 33% - 33% - 33%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="39" y1="15" x2="39" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="20.24" y1="15" x2="20.24" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-4"}], ['cw/column-block', {colClass: "col-4"}], ['cw/column-block', {colClass: "col-4"}]]
    },
    {
        name: "four-column-equal",
        title: "4 kolommen: 25% - 25% - 25% - 25%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="42.09" y1="15" x2="42.09" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="29.62" y1="15" x2="29.62" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="16.67" y1="15" x2="16.67" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-3"}], ['cw/column-block', {colClass: "col-3"}], ['cw/column-block', {colClass: "col-3"}], ['cw/column-block', {colClass: "col-3"}]]
    },
    {
        name: "four-column-unequal",
        title: "4 kolommen: 40% - 20% - 20% - 20%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="15" y1="15" x2="15" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="35" y1="15" x2="35" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="25" y1="15" x2="25" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-6"}], ['cw/column-block', {colClass: "col-2"}], ['cw/column-block', {colClass: "col-2"}], ['cw/column-block', {colClass: "col-2"}]]
    },
    {
        name: "four-column-unequal-inverse",
        title: "4 kolommen: 20% - 20% - 20% - 40%",
        scope: ["block"],
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                <rect x="5" y="15" width="50" height="30" rx="2" fill="#fff" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="35" y1="15" x2="35" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="25" y1="15" x2="25" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
                <line x1="45" y1="15" x2="45" y2="45" fill="none" stroke="#0075b0" stroke-miterlimit="10"/>
            </svg>
            ),
        innerBlocks: [['cw/column-block', {colClass: "col-2"}], ['cw/column-block', {colClass: "col-2"}], ['cw/column-block', {colClass: "col-2"}], ['cw/column-block', {colClass: "col-6"}]]
    },
];

export default variations;
