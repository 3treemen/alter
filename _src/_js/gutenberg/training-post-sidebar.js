const { registerPlugin } = wp.plugins;

import { PluginDocumentSettingPanel } from "@wordpress/edit-post";
import { DatePicker, Button, TextControl } from "@wordpress/components";
import { CwSelectControl, selectControlOptionCreator } from "./components/select_control/CwSelectControl";
import { getMetaValues, setMetaValues } from "./helpers/utilities";
import { MediaUpload, MediaUploadCheck } from '@wordpress/block-editor';

// Component to choose whether or not to show a particular training post in the block
let ShowTrainingControl = (props) => {
	const {showInBlock, onMetaFieldChange} = props;

	const selectOptions = selectControlOptionCreator(
		["Nee", "Ja"],
		["no", "yes"]
	);

    return (
		<CwSelectControl 
			label="Toon in block"
			value={showInBlock}
			options={selectOptions}
			onChange={value => onMetaFieldChange(value)}
		/>
    );
};


let SelectTrainingType = (props) => {
	const {type, onMetaFieldChange} = props;

	const typeOptions = selectControlOptionCreator(
		["Kies", "Open", "In Company"],
		["", "open", "company"]
	);

    return (
		<CwSelectControl 
			label="Soort training"
			value={type}
			options={typeOptions}
			onChange={value => onMetaFieldChange(value)}
		/>
    );
};


// Component to set the start date of a particular training
let StartDateControl = props => {
	const {date, onMetaFieldChange} = props;

	return (
		<DatePicker 
			label="Startdatum"
			currentDate={date}
			onChange={value => onMetaFieldChange(value)}
			is12Hour={true}
		/>
	);
};

// Component to set the duration of a particular training
let DurationControl = props => {
	const {duration, onMetaFieldChange} = props;

	return (
		<TextControl 
			label="Looptijd"
			value={duration}
			onChange={value => onMetaFieldChange(value)}
		/>
	);
};

// Component to set the location of a particular training
let LocationControl = props => {
	const {location, onMetaFieldChange} = props;

	return (
		<TextControl 
			label="Locatie"
			value={location}
			onChange={value => onMetaFieldChange(value)}
		/>
	);
};

// Component to set the cost of a particular training
let CostControl = props => {
	const {cost, onMetaFieldChange} = props;

	return (
		<TextControl 
			label="Prijs"
			type="number"
			value={cost}
			onChange={value => onMetaFieldChange(value)}
		/>
	);
};


// Component to upload a info file
let BrochureControl = props => {
	const {info_file, onMetaFieldChange} = props;

	return (
		<MediaUploadCheck>
			<MediaUpload
				onSelect={ ( value ) => onMetaFieldChange(value.id)}
				value={ info_file }
				render={ ( { open } ) => (
					<Button onClick={ open }>
						Kies een brochure 
					</Button>
				) }
			/>
		</MediaUploadCheck>
	);
};


// Functions to get and set training type in block
SelectTrainingType = getMetaValues("type", "_training_type")(SelectTrainingType);
SelectTrainingType = setMetaValues("_training_type")(SelectTrainingType);

// Functions to get and set training show in block
ShowTrainingControl = getMetaValues("showInBlock", "_training_show_in_block")(ShowTrainingControl);
ShowTrainingControl = setMetaValues("_training_show_in_block")(ShowTrainingControl);

// Functions to get and set training start date
StartDateControl = getMetaValues("date", "_training_start_date")(StartDateControl);
StartDateControl = setMetaValues("_training_start_date")(StartDateControl);

// Functions to get and set training duration
DurationControl = getMetaValues("duration", "_training_duration")(DurationControl);
DurationControl = setMetaValues("_training_duration")(DurationControl);

// Functions to get and set training location
LocationControl = getMetaValues("location", "_training_location")(LocationControl);
LocationControl = setMetaValues("_training_location")(LocationControl);

// Functions to get and set training location
CostControl = getMetaValues("cost", "_training_cost")(CostControl);
CostControl = setMetaValues("_training_cost")(CostControl);

// Functions to get and set training info file
BrochureControl = getMetaValues("info_file", "_training_info_file")(BrochureControl);
BrochureControl = setMetaValues("_training_info_file")(BrochureControl);

registerPlugin( 'training-meta-sidebar', {
    icon: '',
    render: () => {
        return (
            <>
                <PluginDocumentSettingPanel name="training-custom-panel" title="Training instellingen">
					<SelectTrainingType />
                	<ShowTrainingControl />
                    <p className="inspector-control-label" style={{marginTop: 15, marginBottom: 5}}>Startdatum</p>
					<StartDateControl />
					<DurationControl />
					<LocationControl />
					<CostControl />
					<p className="inspector-control-label" style={{marginTop: 15, marginBottom: 5}}>Brochure</p>
					<BrochureControl />
                </PluginDocumentSettingPanel>
            </>
        )
    }
})
