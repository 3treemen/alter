import {registerBlockType} from "@wordpress/blocks";
import ServerSideRender from "@wordpress/server-side-render";
import {InspectorControls} from "@wordpress/block-editor";
import {PanelBody, TextControl} from "@wordpress/components";
import {CwSelectControl, selectControlOptionCreator} from "./components/select_control/CwSelectControl";

registerBlockType("cw/training-block", {
	title: "Training block",
	description: "Gebruik dit blok om een trainingsoverzicht weer te geven. Dit blok wordt weergegeven als een slider op de frontend.",
	icon: "clipboard",
	category: "embed",
	supports: {
		className: false
	},
	attributes: {
		blockTitle: {
			type: "string",
			default: ""
		},
		typeTraining: {
			type: "string",
			default: "all"
		},
		nrItems: {
			type: "string",
			default: "1"
		},
		showSlider: {
			type: "string",
			default: "1"
		}
	},
	//render functions
	edit: props => {

        const { attributes, setAttributes } = props;
        const { blockTitle, nrItems, showSlider, typeTraining } = attributes;

        const itemsOptions = selectControlOptionCreator(
			["1", "2", "3", "4", "5", "6", "7", "8", "All"], 
			["1", "2", "3", "4", "5", "6", "7", "8", "-1"]
		);

		const slideroptions = selectControlOptionCreator(
			["Ja", "Nee"], 
			["1", "0"]
		);

		const typeoptions = selectControlOptionCreator(
			["Alle", "Open", "In Company"], 
			["all", "open", "company"]
		);

        return (
            <>
				<InspectorControls>
					<PanelBody title="Block instellingen" initialOpen={true}>
						<TextControl
							label="Block title"
							value={ blockTitle }
							onChange={ ( blockTitle ) => setAttributes( { blockTitle } ) }
						/>
						<CwSelectControl 
							label="Maximaal aantal items"
							value={nrItems}
							options={itemsOptions}
							onChange={nrItems => setAttributes({nrItems})}
						/>
						<CwSelectControl 
							label="Type trainingen"
							value={typeTraining}
							options={typeoptions}
							onChange={typeTraining => setAttributes({typeTraining})}
						/>
						<CwSelectControl 
							label="Toon als slider"
							value={showSlider}
							options={slideroptions}
							onChange={showSlider => setAttributes({showSlider})}
						/>
					</PanelBody>            
				</InspectorControls>

				<ServerSideRender block="cw/training-block" attributes={ attributes } />            
            </>
        )
    },
    save: props => {
        return null;
    }
})