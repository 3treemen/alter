const {registerBlockType} = wp.blocks
const {addFilter} = wp.hooks

function addListBlockClassName( settings, name ) {
	if ( name === 'core/image') {
        settings.styles = []
	}
	if ( name !== 'cw/row-block') {
        settings.parent = [
            'cw/row-block'
        ]
    }
	return settings;
}
 
addFilter('blocks.registerBlockType', 'cw/overwrite-core-block', addListBlockClassName);

setTimeout(function(){
    const allowedEmbedBlocks = [
        'vimeo',
        'youtube',
    ];
    wp.blocks.getBlockVariations('core/embed').forEach(function (blockVariation) {
        if (-1 === allowedEmbedBlocks.indexOf(blockVariation.name)) {
            wp.blocks.unregisterBlockVariation('core/embed', blockVariation.name);
        }
    });
}, 1500);